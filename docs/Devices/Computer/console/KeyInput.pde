//import java.awt.geom.*;

public class KeyInput 
{ 
String activeInput;  
  KeyInput() 
  { 
    activeInput = fullDirectory; 
    textEngine.addString(activeInput); 
  } 

  void update(char m) 
  { 
    switch(m) 
    { 
    case 8://back space 
      //prevent bs from deleting dir name 
      if (activeInput.length() > fullDirectory.length()) 
      { 
        activeInput = activeInput.substring(0, activeInput.length()-1); 
      } 
      break; 
    case 10: // enter on pc 
    case 13: // enter on mac
      if (yesNo == false)
      {
        parser.sendParse(activeInput);
      } /*else
      {
        yesNoQuestion.check(activeInput);
      }*/
        activeInput = fullDirectory; 
      //- Add a string when new input is given 
      textEngine.addString(activeInput); 
      parser.resetError();
      break; 
    case 65535: 
    case 127: 
    case 27: 
      break; 
    default: 
      if(activeInput.length() < maxLength) 
      { 
        activeInput = activeInput + m; //add char at the end of string 
      } 
      break; 
    } 

    textEngine.setLastString(activeInput);
    OscMessage textinput = new OscMessage("/textinput");
    textinput.add("found");
    oscP5.send(textinput, myRemoteLocation);  
  } 
}
