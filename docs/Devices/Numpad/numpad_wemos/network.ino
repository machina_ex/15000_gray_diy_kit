/*
* Netzwerk Funktionen
*
*
*/

/*
*  UDP Verbindung einrichten
*  Gibt true zurueck wenn erfolgreich
*/
bool connectUDP()
{
    bool state = false;
    
    Serial.println("");
    Serial.println("Connecting to UDP");
    
    if(UDP.begin(localPort) == 1)
    {
      Serial.println("Connection successful");
      state = true;
    }
    else
    {
      Serial.println("Connection failed");
    }
    
    return state;
}

/*
* Mit W-lan verbinden
* gibt true zureuck wenn erfolgreich
*/
bool connectWifi()
{
    boolean state = true;
    int i = 0;
    WiFi.config(ip,gateway,subnet);// optional if we want to get an IP of our choice if possible
    WiFi.begin(ssid, password);
    Serial.println("");
    Serial.println("Connecting to WiFi");
    
    // Wait for connection
    Serial.print("Connecting");
    while (WiFi.status() != WL_CONNECTED) 
    {
      delay(500);
      Serial.print(".");
      if (i > 10){
      state = false;
      break;
      }
      i++;
    }
    if (state)
    {
      Serial.println("");
      Serial.print("Connected to ");
      Serial.println(ssid);
      Serial.print("IP address: ");
      Serial.println(WiFi.localIP());
      
      char myIp[24];
      sprintf(myIp, "%d.%d.%d.%d",ip[0],ip[1],ip[2],ip[3]);
      
          // send hello I'm there to the home base:
          char hello[48];
          sprintf(hello, "15kGy_Clock online as:%s:%d", myIp,localPort);
          sendUDPstr(hello, outIP);

    }
    else 
    {
      Serial.println("");
      Serial.println("Connection failed.");
    }
    return state;
}


/*
*  UDP Nachricht versenden
*/
void sendUDPstr(char* message, IPAddress outIPInput)
{
  UDP.beginPacket(outIPInput, localPort);
  UDP.write(message);
  UDP.endPacket();
}

