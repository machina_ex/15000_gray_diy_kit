

void SerialRead()
{
  
  ///// Serial //////////
  if (incomingByte=Serial.available()>0) 
  {    
      //Serial.print(incomingByte);
      char byteIn = Serial.read();
      cmd[cmdIndex] = byteIn;
      //Serial.println(int(byteIn));
      
      if(byteIn=='\n')
      {
         
        //command finished
        cmd[cmdIndex] = '\0';
        cmdIndex = 0;
        
        String command(cmd);
        command.trim();
        
        if(command == "/START")
        {
          clockRunning = true;
          Serial.println("/START 1");
        }
        else if (command == "/STOP") 
        {
          Serial.println("/STOP 1");
          clockRunning = false;
        }
        else if (command == "/BACKWARDS") 
        {
          backwards = true;
        }
        else if (command == "/FORWARDS") 
        {
          backwards = false;
        }
        else if (command == "/STANDALONE 1") 
        {
          standalone = true;
          Serial.println("/STANDALONE 1");
        }
        else if (command == "/STANDALONE 0") 
        {
          standalone = false;
          Serial.println("/STANDALONE 0");
        }
        else if (command.substring(0,6) == "/SPEED") 
        {
          String commandspeed = command.substring(6,MAX_CMD_LENGTH);
          clockpause = commandspeed.toInt();
        }
        else if (command.substring(0,5) == "/TIME") 
        {
          String commandtime = command.substring(5,MAX_CMD_LENGTH);
          int input = commandtime.toInt();

          if (input > 99 )
           {
             //Serial.println(input);
             minuten = input/100;
             sekunden = input%100;
           }
           else
           {
             sekunden = input;
             minuten = 0;
           }
        }
        else if (command == "/ECHO") 
        {
          Serial.print("/ECHO ");
          Serial.print(displayed);
          Serial.print(" ");
          Serial.println(kabelStatus);
        }
        else if (command == "/SOUND 0") 
        {
          setSound(0);
        }
        else if (command == "/SOUND 1") 
        {
          setSound(1);
        }

   
      }
      else
      {
        if(cmdIndex++ >= MAX_CMD_LENGTH)
        {
          cmdIndex = 0;
        }
      }
      
    }
  
    
}



void setSound(int oN)
{
   soundOn =  oN;   
}


void msgReplyCables(char wort[])
{
  
  String output = "/CABLES ";
  output += wort;
  Serial.println(output);
 
}


