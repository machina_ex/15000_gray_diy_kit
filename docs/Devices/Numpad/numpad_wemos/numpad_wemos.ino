/*
 * Dieser Sketch ist Teil der Bastelanleitung fuer das Real Live Game Adventures
 * 15000 Gray von machina eX.
 * 
 * Spiele ihn auf den WEMOS auf um das Numpad in 15000 Gray einzubinden.
 * 
 * Mehr Informationen und Anleitungen findest du hier:
 * https://www.machinaex.de/15000-gray-diy-kit/
 *
 * Ver.0.1 30.01.2018
 * 
 * Um das Numpad ohne grossen Aufwand nutzen zu koennen binden wir die 
 * keypad library for arduino von Mark Stanley in unseren Code ein:
 * https://playground.arduino.cc/Code/Keypad
 * Installiere die Library über Sketch -> include library -> manage libraries
 * tippe "keypad" in das Suchfeld ein und installiere die Keypad Library von Mark Stanley
 */

// Die Netzwerk Library einbinden
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

// Die Keypad Library einbinden
#include <Keypad.h>

/* 
 *  Variablen fuer die Verbindung zum Netzwerk
 *  Setze hier deine WLAN Verbindungsdaten ein.
 */
const char* ssid = "deineWiFiID";
const char* password = "deinWiFiPasswort";

bool wifiConnected = false;


/* 
 *  Variablen fuer die Netzwerkkommunikation
 *  setze hier die IP Adressen abhaengig von deinen Netzwerkeinstellungen ein.
 */
IPAddress ip(192,168,178,112); // Die IP Adresse der Uhr
IPAddress gateway(192,168,178,1); // Die IP Adresse des Routers
IPAddress subnet(255,255,255,0); // Die Subnetzmaske. Fuer gewoehnlich (255,255,255,0)
IPAddress outIP (192,168,178,51); // Die IP Adresse der Zentralen Schnittstellen Software

unsigned int localPort = 8888; // Der Kommunikationsport
WiFiUDP UDP;
bool udpConnected = false;
char packetBuffer[UDP_TX_PACKET_MAX_SIZE]; //Zwischenspeicher fuer eingehende Daten
char ReplyBuffer[] = "acknowledged"; // Bestaetigungsnachricht

/* 
 *  Variablen fuer das Keypad
 */
const byte ROWS = 4; // Vier Reihen
const byte COLS = 3; // Drei Zeilen
char keys[ROWS][COLS] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'#','0','*'}
};
//byte rowPins[ROWS] = {0, 2, 14, 12}; // mit den Reihen Pins verbunden
//byte colPins[COLS] = {16, 5, 4}; // mit den Zeilen Pins verbunden

byte rowPins[ROWS] = {D3, D4, D5, D6}; // mit den Reihen Pins verbunden
byte colPins[COLS] = {D0, D1, D2}; // mit den Zeilen Pins verbunden

// Keypad Objekt aus Keypad Library erstellen
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

// Digital Pin Variablen fuer die LEDs
int gruen = D7;
int rot = D8;

// string zum speichern der letzten drei Eingaben
char letzteEingabe[4] = "xxx";

void setup(){
  Serial.begin(115200);
  Serial.print("15000 Gray Numpad ...");

  // Wifi Verbindung herstellen
  wifiConnected = connectWifi();
   
  // ueberpruefen ob eine Verbindung hergestellt werden konnte
  if(wifiConnected)
  {
    udpConnected = connectUDP();
    if (udpConnected)
    {
        Serial.println("... verbindung hergestellt");
        sendUDPstr("connected", outIP);
    }
  }

  pinMode(gruen, OUTPUT);
  pinMode(rot, OUTPUT);

  // Die LEDs eine Sekunde aufleuchten lassen
  digitalWrite(gruen, HIGH);
  digitalWrite(rot, HIGH);

  delay(1000);

  digitalWrite(gruen, LOW);
  digitalWrite(rot, LOW);
}

void loop(){
  char key = keypad.getKey();

  if (key != NO_KEY){
    // Die chars (Buchstaben) im letzteEingabe String um eine Stelle nach links verschieben
    letzteEingabe[0] = letzteEingabe[1];
    letzteEingabe[1] = letzteEingabe[2];
    letzteEingabe[2] = key;
    
    Serial.println(letzteEingabe);
    sendUDPstr(letzteEingabe, outIP);

    // Die letzte Eingabe mit dem Loesungscode vergleichen
    if(strcmp(letzteEingabe, "169") == 0)
    {
      Serial.println("Numpad geloest");
      sendUDPstr("solved", outIP);
      digitalWrite(gruen, HIGH);
      digitalWrite(rot, LOW);
    } else {
      digitalWrite(rot, HIGH);
      digitalWrite(gruen, LOW);
    }
  }
}
