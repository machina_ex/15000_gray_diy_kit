//import java.awt.geom.*;

public class Pong
{
int noteDuration = 100; // Duration of each note in milliseconds
float[] rawSequence = { 800, 550, 100 }; // right and left paddle impact, and scoreIncrease pitches

int xDirection = 1;
int yDirection = 1;
//char keyp = 'S';
int circleDia = 10; // Diameter of the circle/square
float circleRad = circleDia/2; // Radius of the circle/square
float xBallPos, yBallPos, lPaddleHeight, rPaddleHeight;// Variables for the position of the ball and the paddles
int paddleWidth = 80;
int paddleHeight = 10;
int heart = 160;
float origSpeed = 10;
float speed = origSpeed; // Speed of the ball.  This increments as the game plays, to make it move faster
float speedInc = origSpeed/15;
float ySpeed = 0; // Start the ball completely flat.  Position of impact on the paddle will determine reflection angle.
int paused = 1; // Enable pausing of the game.  Start paused
float def = random (100); // Deflection of the Ball

int paddleJump = 10; // How fast the paddles move every frame

//int multiPress = 0; // Initial setting of key states, ie 0 = nothing pressed
//final static int LEFTUP = 1; // Use bitwise 'or' to track multiple key presses
//final static int LEFTDOWN = 2; 
//final static int RIGHTUP = 4; 
//final static int RIGHTDOWN = 8; 

Pong()
{background(0);
  smooth();
  noStroke();
  //stroke(120, 240, 40);
  scoreFont = loadFont("OCRAStd-10.vlw"); // Score font
  playerFont = loadFont("OCRAStd-10.vlw"); // Player font
  //frameRate(60);
  xBallPos = width/2; // Start the ball in the centre
  yBallPos = height/2;
  lPaddleHeight = width+40;
  rPaddleHeight = width+80;
}

void drawYourself()
{
rectMode(CORNER);
  fill(0); // Second argument can be added to create a slight motion blur when the ball moves.
  rect(0, 0, width, height);
  
  // Handle the possible multiple key presses
    switch(multiPress) { 
    case LEFTUP: if (lPaddleHeight >= 40) {lPaddleHeight -= paddleJump;} break; 
    case LEFTDOWN: if (lPaddleHeight <= width) {lPaddleHeight += paddleJump;} break; 
    case RIGHTUP: if (rPaddleHeight >= 40) {rPaddleHeight -= paddleJump;} break; 
    case RIGHTDOWN: if (rPaddleHeight <= width) {rPaddleHeight += paddleJump;} break; 
    case LEFTUP|RIGHTUP: if (lPaddleHeight >= 40) {lPaddleHeight -= paddleJump;} if (rPaddleHeight >= 40) {rPaddleHeight -= paddleJump;} break; 
    case LEFTUP|RIGHTDOWN: if (lPaddleHeight >= 40) {lPaddleHeight -= paddleJump;} if (rPaddleHeight <= width) {rPaddleHeight += paddleJump;} break; 
    case LEFTDOWN|RIGHTUP: if (lPaddleHeight <= width) {lPaddleHeight += paddleJump;} if (rPaddleHeight >= 40) {rPaddleHeight -= paddleJump;} break; 
    case LEFTDOWN|RIGHTDOWN: if (lPaddleHeight <= width) {lPaddleHeight += paddleJump;} if (rPaddleHeight <= width) {rPaddleHeight += paddleJump;} break; 
  }

   if (rPaddleHeight > width-40) {
    rPaddleHeight = -40;
  }
  
   if (lPaddleHeight > width) {
    lPaddleHeight = -40;
  }

// The middle line or 'net'
  rectMode(CENTER);
  for (int i = 0; i < 17; i++) {  // There should be an odd number of rects to make up the middle line
    if(i % 2 == 0) {
      fill(255);
    } else {
      fill(255,0);
    }
    rect(i*width/16, height/2, height/16, 10); // The height of each is one less than the max number of iterations of 'for' loop as we are drawing rectMode(CENTER)
  }
  
// Show current scores
//textAlign(RIGHT, TOP);
textFont(playerFont);
textSize(10);
fill (120,240,40);
text("Hz", width-30, 35);
textFont(scoreFont);
textSize(20);
fill (120,240,40);
text(heart, width-40, 25);
textFont(playerFont);
textSize(16);
fill (120,240,40);
text("play with q + w and o +p", width-300, 598);


// Draw some paddles
  rectMode(CENTER);
  rect(lPaddleHeight, 15, paddleWidth, paddleHeight); // Left Paddle
  rect(rPaddleHeight, height-15, paddleWidth, paddleHeight); // Right Paddle
  

  // Define the boundaries
  if ((yBallPos > height-circleRad-20) && ((xBallPos >= rPaddleHeight-paddleWidth/2) && (xBallPos <= rPaddleHeight+paddleWidth/2))) { // test to see if it is touching right paddle
    yDirection = -1; // Make the ball move from right to left
    ySpeed = (rPaddleHeight + xBallPos) / 220; // Make position of impact on paddle determine deflection angle.  Not perfect.
    speed -= speedInc;
    heart -= 10;
    OscMessage pong = new OscMessage("/pong");
    pong.add("pongwin");
    oscP5.send(pong, myRemoteLocation); 
  }
  
  if ((yBallPos < circleRad+20) && ((xBallPos >= lPaddleHeight-paddleWidth/2) && (xBallPos <= lPaddleHeight+paddleWidth/2))) { // test to see if it is touching left paddle
    yDirection = 1; // Make the ball move from left to right  
    ySpeed = (lPaddleHeight + xBallPos) / 220; // Make position of impact on paddle determine deflection angle.  Not perfect.
    speed -= speedInc;
    heart -= 10;
    OscMessage pong = new OscMessage("/pong");
    pong.add("pang");
    oscP5.send(pong, myRemoteLocation); 
  }
  
  if ((yBallPos > height-circleRad) || (yBallPos < circleRad)) {
    yDirection = -yDirection;
    ySpeed = xBallPos / 200;
    speed = origSpeed;
    OscMessage pong = new OscMessage("/pong");
    pong.add("ping");
    oscP5.send(pong, myRemoteLocation); 
    if (heart < 250) {
    heart += 10;}
  } 
  if (xBallPos < circleRad) {
    xDirection = -xDirection;
    speed = origSpeed;
    if (heart < 250) {
    heart += 10;}
  } 
  
 if (xBallPos > width-circleRad)
 {
    xBallPos = 0 + circleRad;
 }  
  
 if (yBallPos > height+circleRad) { // If the ball goes off the screen, reset it to the centre.
    speed = origSpeed;
    xBallPos = width/2;
    ySpeed = random(-1., 1.);
    yBallPos = height/2;
  }
  
  if (yBallPos < 0-circleRad) {
    speed = origSpeed;
    xBallPos = width/2;
    ySpeed = random(-1., 1.);
    yBallPos = height/2;
  }
  
  // Draw the ball
  fill(100, 200, 30);
  rect(xBallPos, yBallPos, circleDia, circleDia);
  
  // Draw the trace
  
 if (yDirection == 1) {
  for (int i = 1; i < height/10-3; i++) {  
  fill(120, 240, 30, 255-i*5);
  rect(xBallPos-i, yBallPos-i*10, circleDia, circleDia);
  }
  }
 if (yDirection == -1) {
  for (int i = 1; i < height/10-3; i++) {
  fill(120, 240, 30, 255-i*5);  
  rect(xBallPos-i, yBallPos+i*10, circleDia, circleDia);
  }
  }
   
  // Current ball position
  xBallPos = xBallPos+ySpeed*xDirection;
  yBallPos = yBallPos+speed*yDirection;
  if (heart < 70) {
   OscMessage pong = new OscMessage("/pong");     
   pong.add("pongwin");
   oscP5.send(pong, myRemoteLocation); 
   state = 0;
   heart = 160;
   }
   }
   
}
