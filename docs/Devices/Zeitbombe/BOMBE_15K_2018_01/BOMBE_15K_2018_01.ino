/*
 * TIMEBOMB Prop by machina eX
 * 
 * code by Philip Steimel
 * developed by Lasse Marburg and Philip Steimel
 * 
 * licence: Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
 * 
 * for commercial use please contact philip @ machinaex . de
 * 
 * Basecode for auf Arduino laufende Bombe mit Sparfun 7-Segment Display auf Arduino Uno
 * talking with WEMOS D1 mini via Serial
 *
 * Serial Befehle (Strings + Lineend via Serialinput):
 * /SPEED <wishedSpeedInMillisecs>
 * /TIME <wishedNumberToDisplay>
 * /START ; /STOP ; /FORWARDS ; /BACKWARDS, /SOUND 1; /SOUND 0; 
 * 
 * features for the future:
 * - SPI connection to WEMOS instead of Serial
 * - proper Soundcontrol
 * 
 * more information and hardware buildplan at:
 * https://bitbucket.org/machinaex/15000-gray-diy-kit/src
 * or
 * https://www.machinaex.de/15000-gray-diy-kit/
 * 
*/

#include <Wire.h> // for talking to Display via i2c (could instead run via SPI)

//Standalone variables
boolean standalone = false;
boolean standaloneAlt = false;
boolean bombIsHot = false;
boolean bombIsHotalt = false;
boolean gameOver = false;
boolean pushCables = true;

#define DISPLAY_ADDRESS1 0x71 //This is the default address of the OpenSegment with both solder jumpers open
#define OUT_1 2 
#define OUT_2 3 
#define OUT_3 4
#define OUT_4 5
//#define OUT_5 11

#define IN_1 6
#define IN_2 7
#define IN_3 8
#define IN_4 9
//#define IN_5 11

#define OUT_PIN 13 // the testled in case you wanna see what you are doing IRL

#define SOUNDPIN 10
#define SOUNDDELAY 150

#define DEFUSED "1234"

//Serial variables
int MAX_CMD_LENGTH = 14;
char cmd[14];
int cmdIndex;
char incomingByte;

//clock variables
int sekunden = 16;
int sekundenAlt = 0;
int minuten = 20;
boolean backwards = true;
boolean clockRunning = false;
long clockpause = 1000;
long now = 0;
long lastUpdate = 0;
long lastupdateCables = 0;

//display variables
int displayed = 2016;

//kabelraetsel variables
String kabelStatus = ""; // query and send this var for outside communication of cable puzzle
String kabelStatusAlt = "";
int in_1_status = 0;
int in_2_status = 0;
int in_3_status = 0;
int in_4_status = 0;
boolean kabelSolved = false;
boolean kabelSolvedAlt = false;


//sound vars
boolean soundOn = false;
int speakerPin = 10;

char notes[] = "g ahce g ahce g ahce g ahce ga Cbagfc"; // a space represents a rest
int notesLoose[] = { 2093 ,1568, 1318, 1760, 1975, 1760, 1661, 1864,1661, 0, 2637, 2349, 2637};
int length = 37; // the number of notes8
int lengthLoose = 13;
int beats[] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 4};
int beatsLoose[] = { 4,4,4,4,8,2,2,2,2,4,1,1,8};
int tempo = 100;


void setup()
{

  Serial.begin(9600); //

  //Kabelrätsel Pins
  pinMode (IN_1, INPUT_PULLUP);  pinMode (OUT_1, OUTPUT);
  pinMode (IN_2, INPUT_PULLUP);  pinMode (OUT_2, OUTPUT);
  pinMode (IN_3, INPUT_PULLUP);  pinMode (OUT_3, OUTPUT);
  pinMode (IN_4, INPUT_PULLUP);  pinMode (OUT_4, OUTPUT);
  //pinMode (IN_5, INPUT_PULLUP);

  Wire.begin(); //Join the bus as master

  //Send the reset command to the display - this forces the cursor to return to the beginning of the display
  Wire.beginTransmission(DISPLAY_ADDRESS1);
  Wire.write('v');
  Wire.endTransmission();
}



void loop()
{

  SerialRead();

  sekundencounter();
  minutenAbhaengigVonSekunden();
  minutencounter();

  //displaying numbers:
  displayed = minuten * 100 + sekunden;

  now = millis();

  if ( lastUpdate + clockpause < now )
  {

    if (backwards && clockRunning)
    {
      sekunden--;
    }
    if (!backwards && clockRunning)
    {
      sekunden++;
    }

    lastUpdate = now;

    if (!kabelSolved && clockRunning && soundOn)
    {
      tone(SOUNDPIN, 960);
      kabelSolvedAlt =  kabelSolved;
    }

    if (gameOver && soundOn)
    {
      noTone(SOUNDPIN);
      clockRunning = false;
      gameOver = false;

      Serial.println("/LOOSE");
      //msgReplyStr("LOOSE");
      melodieLoose();
      
      //delay(3000);
      //reset:
      //reset(); // not working properly on UNO to be checked

    }
    if (gameOver && !soundOn)
    {
      clockRunning = false;
      gameOver = false;

      Serial.println("/LOOSE_MUTE");
    }

     if (kabelSolved && !kabelSolvedAlt)
     {
       Serial.println("/WIN");
       //msgReplyStr("WIN");
       if (soundOn){
        noTone(SOUNDPIN);
        melodie();
       }
       kabelSolvedAlt = kabelSolved;
     }


  }
  if ( lastUpdate + SOUNDDELAY < now )
  {
    noTone(SOUNDPIN);
  }

  //beschreibt das 7-Segment (akzeptiert Ints only)
  i2cSendValue(displayed);

  kabelRaetsel();

  standaloneManager();

}


void melodie()
{
  for (int i = 0; i < length; i++)
  {
    if (notes[i] == ' ') {
      delay(beats[i] * tempo); // rest
    } else {
      playNote(notes[i], beats[i] * tempo);
    }

    // pause between notes
    delay(tempo / 2);
  }
}

void melodieLoose()
{
  for (int i = 0; i < lengthLoose; i++)
  {
    if (notesLoose[i] == 0) {
      delay(beatsLoose[i] * tempo); // rest
    } else {
      playTone(notesLoose[i], beatsLoose[i] * tempo);
    }

    // pause between notes
    delay(tempo / 2);
  }
}

void reset()
{
  minuten = 7;
  sekunden = 1;
  bombIsHot = true;
  kabelSolved = false;
  kabelSolvedAlt = false;
}
