// ======= Falling Piece Class ===============
// ===========================================
public class fallingPiece{
  int pieceUsed;
  int posCreating;
  color pieceCol;
  int[][] brickPos = new int[4][2]; // [0]=x  [1]=y 
  
  // These boolean arrays are used to create the pieces
  // however after that a piece is just a set of 4 x and y values
  // so really it is four pieces. 

  boolean[][] I_PIECE = {
    { true,  true,  true,  true   }
  };

  boolean[][] J_PIECE = {
    { true, false, false  } ,
    {  true, true,  true    } 
  };

  boolean[][] L_PIECE = {
    {  false, false, true      },
    {  true,  true,  true  }
  };

  boolean[][] O_PIECE = {
    { true, true  },
    { true, true }
  };

  boolean[][] S_PIECE = {
    { false, true, true },
    { true,  true, false    }
  };

  boolean[][] T_PIECE = {
    { false, true, false   },
    {  true,  true, true  }
  };

  boolean[][] Z_PIECE = {
    {  true,  true, false   } ,
    {  false, true, true   }
  };

  color[] pieceColors = {
    color(120,240,40),color(120,240,40), color(120,240,40), 
    color(120,240,40), color(120,240,40), color(120,240,40), color(120,240,40) };

  boolean[][][] allPieces = {
    I_PIECE, J_PIECE, L_PIECE, O_PIECE, S_PIECE, T_PIECE, Z_PIECE
  };

    // Pieces are rotated according to their relationship to the center brick
  int[] centerPiece = {
    1, 2, 1, 1, 2, 2, 2                                      };

  // Constructor
  fallingPiece(int pieceUsed){  
    int[][] brickPosTemp = new int[4][2];
    this.pieceUsed = pieceUsed;
    pieceCol = pieceColors[pieceUsed];

    for (int j=0; j<allPieces[pieceUsed][0].length; j++){
      for (int i=0; i<allPieces[pieceUsed].length; i++){

        if(allPieces[pieceUsed][i][j]){
          brickPosTemp[posCreating%4][0] = (numCol/2)-1 + i;
          brickPosTemp[posCreating%4][1] =  j;
          if (myBoardBlock[brickPosTemp[posCreating%4][0]][brickPosTemp[posCreating%4][1]].isTaken() == true){
            myBoard.gameOver();
            break;
          }
          posCreating ++;
        }
      }
      if (gameIsOver == true){
        break;
      }
    }
    if (gameIsOver == false){
      for (int i=0; i<brickPos.length; i++){
        brickPos[i][0] = brickPosTemp[i][0];
        brickPos[i][1] = brickPosTemp[i][1];
      }
    }
  }



  // Functions
  void render(){
    strokeWeight(1);


    for (int i=0; i<brickPos.length; i++){
      fill(pieceCol);
      stroke (0);
      rect(
      brickPos[i][0]*brickSize+offset, 
      brickPos[i][1]*brickSize+offset, 
      brickSize,brickSize);
    } 
  } 

  void animate(){
    if(gameIsOver == false){
      if(frameCount%(max(10,(25-numRowsCleared))) == 0 && this.canItMove()){
        this.moveY();
      }
      if(keyPressed){
        if (key == CODED){
          if(keyCode == RIGHT && this.canItMoveR()){
            this.moveRight();
            /*OscMessage mytetris = new OscMessage("/tetris");
            mytetris.add("turn");
            oscP5.send(mytetris, myRemoteLocation);*/
            keyPressed = false;
          }
          else if(keyCode == LEFT && this.canItMoveL()){
            this.moveLeft();
            /*OscMessage mytetris = new OscMessage("/tetris");
            mytetris.add("turn");
            oscP5.send(mytetris, myRemoteLocation);*/
            keyPressed = false;
          }
          else if(keyCode == DOWN && this.canItMove()){
            this.moveY();
            /*OscMessage mytetris = new OscMessage("/tetris");
            mytetris.add("fullrow");
            oscP5.send(mytetris, myRemoteLocation);*/
            keyPressed = false;
          }
          else if(keyCode == UP){
            OscMessage mytetris = new OscMessage("/tetris");
            mytetris.add("turn");
            oscP5.send(mytetris, myRemoteLocation);
            this.rotate();
            keyPressed = false;
          }
        }
        else if(key == ' '){
          while (myPiece.canItMove()){
            this.moveY();
          }
          keyPressed = false;
        }
      }
      this.render();
    }
  }


  void moveY(){
    for (int i=0; i<brickPos.length; i++){
      brickPos[i][1] ++; 
    }
  }

  void moveRight(){
    for (int i=0; i<brickPos.length; i++){
      brickPos[i][0] ++; 
    }
  }


  void moveLeft(){
    for (int i=0; i<brickPos.length; i++){
      brickPos[i][0] --; 
    }
  }


  boolean canItMove(){
    for (int i=0; i<brickPos.length; i++){

      if(brickPos[i][1] == numRows-1){
        this.freezeBlock();
        OscMessage mytetris = new OscMessage("/tetris");
        mytetris.add("drop");
        oscP5.send(mytetris, myRemoteLocation);
        return false;
      }
      if(myBoardBlock[brickPos[i][0]][brickPos[i][1]+1].isTaken() == true){
        OscMessage mytetris = new OscMessage("/tetris");
        mytetris.add("drop");
        oscP5.send(mytetris, myRemoteLocation);
        this.freezeBlock();
        return false;
      } 
    }
    return true;
  }


  boolean canItMoveL(){
    for (int i=0; i<brickPos.length; i++){
      if(brickPos[i][0] == 0){
        return false;
      }
      if(myBoardBlock[brickPos[i][0]-1][brickPos[i][1]].isTaken() == true){
        return false;
      } 
    }
    return true;
  }


  boolean canItMoveR(){
    for (int i=0; i<brickPos.length; i++){
      if(brickPos[i][0] == numCol-1){
        return false;
      }
      if(myBoardBlock[brickPos[i][0]+1][brickPos[i][1]].isTaken() == true){
        return false;
      } 
    }
    return true;
  }


  boolean canItRotate(int x, int y){
    if (x > numCol-1
      || x < 0
      || y > numRows-1
      || myBoardBlock[x][y].isTaken() == true 
      ){
      return false;
    }
    return true;
  }


  void freezeBlock(){
    for (int i=0; i<brickPos.length; i++){
      myBoardBlock[brickPos[i][0]][brickPos[i][1]].take();
      myBoardBlock[brickPos[i][0]][brickPos[i][1]].col = pieceColors[pieceUsed];
    }
    myPiece = new fallingPiece((int)random(6.9999));
  }


  void rotate(){
    int[][] brickPosTemp = new int[4][2];
    boolean isItGoingToRotate = true;

    for (int i=0; i<brickPos.length; i++){
      if( i != centerPiece[pieceUsed] && pieceUsed != 3){
        int xDifference;
        int yDifference;
        int temp;

        xDifference = brickPos[i][0] - brickPos[centerPiece[pieceUsed]][0];
        yDifference = brickPos[i][1] - brickPos[centerPiece[pieceUsed]][1];

        xDifference *= -1;
        
        // rotate and check if new position is taken
        brickPosTemp[i][1] = brickPos[centerPiece[pieceUsed]][1] + xDifference;
        brickPosTemp[i][0] = brickPos[centerPiece[pieceUsed]][0] + yDifference;  
        if(canItRotate(brickPosTemp[i][0], brickPosTemp[i][1]) == false){
          isItGoingToRotate = false;
          break;
        }   
      }
    }
    
    // Actually rotate
    if (isItGoingToRotate == true){
      for (int i=0; i<brickPos.length; i++){
        if( i != centerPiece[pieceUsed] && pieceUsed != 3){
          brickPos[i][1] = brickPosTemp[i][1];
          brickPos[i][0] = brickPosTemp[i][0];
        }
      }
    }
    isItGoingToRotate = true;
  }


} 
