15000 Gray DIY Kit
--------------------

Hallo!

Mit dem 15000 Gray DIY Kit hältst du hier eine umfassende Bastelanleitung für ein theatrales Live-point-n-Click-Adventure in den Händen.

Die ZuschauerInnen (d.h. SpielerInnen) befinden sich mit den SchauspielerInnen in einem einfach ausgestatteten Raum und rätseln sich durch eine aufregende Science-Fiction Story:

Professor Hövel hat in seinem Labor ein Verfahren entdeckt, Lebewesen strahlenresistent zu machen. Leider drohen die Ergebnisse aber in die falschen Hände zu geraten. Es liegt nun an den SpielerInnen Professor Hövels Leben und nebenbei die ganze Welt zu retten.

![machinaex_spielerinnen](http://machinaex.de/wp-content/uploads/2013/08/15.000-101-1024x682.jpg "Es gibt immer einen Code - HH")
*Foto: Robin Junicke*

Das 15000 Gray DIY Kit enthält sämtliche Elemente, um das technikbasierte Theatergame nachzubauen: Bühnenpläne, Bastelanleitungen für interaktive Requisiten, Codes zur Schnittstellenprogrammierung, ein Live-Set für Sounds und Musik, natürlich das Script mit Text, Rätselbeschreibungen und Regieanweisungen und vieles mehr.

>Leider wirst du bald feststellen, dass diese Anleitung noch an allen Ecken und Enden Löcher hat. Die versuchen wir wann immer wir Zeit finden zu flicken, hoffen aber auf deine Geduld und Schaffensgeist dort mit Eigeninitiative in die Bresche zu springen wo noch etwas fehlt, unvollständig oder fehlerhaft ist.

Das Material und die Anleitungen stellen wir dir ganz im Sinne des open source-Gedanken gratis zur Verfügung. Ob du nun die ganze Inszenierung realisieren willst oder dich nur für einzelne Problemlösungen interessierst: Fühl dich frei alles nach deinen eigenen Vorstellungen umzubauen und anzupassen.
Das 15000 Gray DIY Kit richtet sich an Interessierte verschiedenster Art: Theater- und Medien-AGs, BastlerInnen, ProgrammiererInnen, TheaterenthusiastInnen, RegisseurInnen, GameentwicklerInnen, etc.

Beim Durchforschen, Ausprobieren und Nachbauen wünschen wir dir viel Spass und stehen gerne für Fragen und Anregungen zur Verfügung. Also, ran an Lötkolben, Rechner und Regietisch: 3, 2, 1…. Go!

*Deine machina eX*

![machinaex_spielerinnen](http://machinaex.de/wp-content/uploads/2013/08/laura-trank3.jpg "Ich brauche eine Lösung - AA")
*Foto: Robin Junicke*

Wenn du wissen willst was wir sonst so machen, besuch uns auf unserer Webseite [www.machinaex.de](http://www.machinaex.de) oder schreib uns eine email an maschinenliebe@machinaex.de


### RAUMPLAN / BÜHNENPLAN

-- Übersichtsskizze -- -- work in progress --

![Bühnenplan](Ausstattung/stageplan.png "Bühnenplan Skizze")
