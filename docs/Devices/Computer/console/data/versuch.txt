_______________________________Notizen_____________________________________
Testreihe 4 - Experiment 5 

Tag 1

Erste Injektion für Ronny und Paula. Die Viecher tun mir jetzt schon 
Leid.

Tag 3

Nach der dritten Injektion keine sichtbare körperliche Reaktion. 
Ronny ist nach wie vor sehr aktiv.

Tag 10

Nach Injektion #8 erste Tumorbildung bei Paula. Linksseitige 
Lähmungen. Ronny noch wohlauf. 

Tag 16

Paula sieht schlecht aus. Nahrungsaufnahme verweigert. 
Ronny nach Injektion #14 sichtbar müde, aber aufrecht. 
Die Hoffnung wächst.

___________________________________________________________________________