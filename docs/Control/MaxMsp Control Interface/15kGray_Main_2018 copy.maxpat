{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 6,
			"minor" : 1,
			"revision" : 6,
			"architecture" : "x86"
		}
,
		"rect" : [ 56.0, 44.0, 1255.0, 960.0 ],
		"bglocked" : 0,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 0,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 0,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"boxanimatetime" : 200,
		"imprint" : 0,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"boxes" : [ 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-46",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1241.75, 884.0, 32.5, 18.0 ],
					"text" : "1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-44",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1160.75, 855.0, 81.0, 20.0 ],
					"text" : "route RESET"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-43",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "bang" ],
					"patching_rect" : [ 1304.0, 822.0, 34.0, 20.0 ],
					"text" : "t 0 b"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-42",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1303.5, 904.0, 54.0, 20.0 ],
					"text" : "gate 1 1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-40",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1338.5, 859.0, 84.0, 18.0 ],
					"text" : "UHRSOLVED"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-36",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "" ],
					"patching_rect" : [ 1290.5, 784.706482, 46.0, 20.0 ],
					"text" : "sel 0 1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1290.5, 757.706482, 185.0, 20.0 ],
					"text" : "route /UHRTRIGGER/GELOEST"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-19",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1290.5, 724.706482, 99.0, 20.0 ],
					"text" : "udpreceive 8000"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-14",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1091.75, 800.0, 150.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 35.75, 473.785889, 84.25, 20.0 ],
					"text" : "print EVENTS"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-10",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1019.372681, 767.706482, 20.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 12.372681, 473.785889, 20.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1019.372681, 800.0, 54.0, 20.0 ],
					"text" : "gate 1 1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-28",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1571.0, 904.0, 83.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1163.5, 792.0, 83.0, 18.0 ],
					"text" : "KABELRAUS",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-27",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1457.0, 904.0, 106.0, 18.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1152.0, 756.0, 106.0, 18.0 ],
					"text" : "KABELSTECKEN",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-25",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1303.5, 964.908936, 60.0, 20.0 ],
					"text" : "s EVENT",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-23",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1150.5, 973.0, 32.5, 18.0 ],
					"text" : "0",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-22",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1233.5, 973.0, 32.5, 18.0 ],
					"text" : "0",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-21",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1201.0, 973.0, 32.5, 18.0 ],
					"text" : "1",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-20",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1118.0, 973.0, 32.5, 18.0 ],
					"text" : "1",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1201.0, 1018.0, 60.0, 60.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1235.0, 665.0, 60.0, 60.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1118.0, 1018.0, 60.0, 60.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1152.0, 665.0, 60.0, 60.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-12",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1201.0, 998.0, 60.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1235.0, 645.0, 60.0, 20.0 ],
					"text" : "RONNY",
					"textcolor" : [ 0.63626, 0.615254, 0.513134, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-11",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1118.0, 998.0, 60.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1152.0, 645.0, 60.0, 20.0 ],
					"text" : "PAULA",
					"textcolor" : [ 0.63626, 0.615254, 0.513134, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 4,
					"outlettype" : [ "bang", "bang", "bang", "" ],
					"patching_rect" : [ 1125.0, 924.380188, 157.0, 20.0 ],
					"text" : "sel PAULA RONNY RESET",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1125.0, 828.206482, 58.0, 20.0 ],
					"text" : "r EVENT",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1019.372681, 828.206482, 77.0, 20.0 ],
					"text" : "print EVENT",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1054.372681, 767.706482, 58.0, 20.0 ],
					"text" : "r EVENT",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 14.0,
					"id" : "obj-38",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patcher" : 					{
						"fileversion" : 1,
						"appversion" : 						{
							"major" : 6,
							"minor" : 1,
							"revision" : 6,
							"architecture" : "x86"
						}
,
						"rect" : [ 76.0, 69.0, 640.0, 480.0 ],
						"bglocked" : 0,
						"openinpresentation" : 0,
						"default_fontsize" : 12.0,
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"gridonopen" : 0,
						"gridsize" : [ 15.0, 15.0 ],
						"gridsnaponopen" : 0,
						"statusbarvisible" : 2,
						"toolbarvisible" : 1,
						"boxanimatetime" : 200,
						"imprint" : 0,
						"enablehscroll" : 1,
						"enablevscroll" : 1,
						"devicewidth" : 0.0,
						"description" : "",
						"digest" : "",
						"tags" : "",
						"boxes" : [ 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.595187,
									"id" : "obj-11",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 285.5, 349.064789, 208.0, 17.0 ],
									"text" : "script sendbox chain_modul hidden $1",
									"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.595187,
									"id" : "obj-4",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 323.0, 251.611115, 32.5, 19.0 ],
									"text" : "!= 3",
									"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.595187,
									"id" : "obj-5",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 295.5, 316.064789, 215.0, 17.0 ],
									"text" : "script sendbox control_modul hidden $1",
									"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.595187,
									"id" : "obj-18",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 50.0, 123.916656, 202.0, 17.0 ],
									"text" : "script sendbox light_modul hidden $1",
									"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.595187,
									"id" : "obj-17",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 58.0, 203.453705, 212.0, 17.0 ],
									"text" : "script sendbox sound_modul hidden $1",
									"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.595187,
									"id" : "obj-10",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 77.0, 233.074066, 32.5, 19.0 ],
									"text" : "!= 2",
									"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.595187,
									"id" : "obj-9",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 50.0, 156.537018, 32.5, 19.0 ],
									"text" : "!= 1",
									"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.595187,
									"id" : "obj-8",
									"maxclass" : "newobj",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 50.0, 93.0, 32.5, 19.0 ],
									"text" : "!= 0",
									"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"fontname" : "Arial",
									"fontsize" : 11.595187,
									"id" : "obj-12",
									"maxclass" : "message",
									"numinlets" : 2,
									"numoutlets" : 1,
									"outlettype" : [ "" ],
									"patching_rect" : [ 77.0, 261.990753, 220.0, 17.0 ],
									"text" : "script sendbox devices_modul hidden $1",
									"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-28",
									"maxclass" : "inlet",
									"numinlets" : 0,
									"numoutlets" : 1,
									"outlettype" : [ "int" ],
									"patching_rect" : [ 198.5, 32.0, 25.0, 25.0 ]
								}

							}
, 							{
								"box" : 								{
									"comment" : "",
									"id" : "obj-33",
									"maxclass" : "outlet",
									"numinlets" : 1,
									"numoutlets" : 0,
									"patching_rect" : [ 45.0, 437.064789, 25.0, 25.0 ]
								}

							}
 ],
						"lines" : [ 							{
								"patchline" : 								{
									"destination" : [ "obj-12", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-10", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-11", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-12", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-17", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-18", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-10", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-4", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-8", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-9", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-28", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-11", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"midpoints" : [ 332.5, 309.337952, 295.0, 309.337952 ],
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-5", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-4", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-33", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-5", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-18", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-8", 0 ]
								}

							}
, 							{
								"patchline" : 								{
									"destination" : [ "obj-17", 0 ],
									"disabled" : 0,
									"hidden" : 0,
									"source" : [ "obj-9", 0 ]
								}

							}
 ]
					}
,
					"patching_rect" : [ 1019.372681, 704.678406, 97.0, 22.0 ],
					"saved_object_attributes" : 					{
						"default_fontface" : 0,
						"default_fontname" : "Arial",
						"default_fontsize" : 12.0,
						"description" : "",
						"digest" : "",
						"fontface" : 0,
						"fontname" : "Arial",
						"fontsize" : 12.0,
						"globalpatchername" : "",
						"tags" : ""
					}
,
					"text" : "p patcherview",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"border" : 1,
					"bordercolor" : [ 0.67731, 0.67731, 0.67731, 0.8 ],
					"borderoncolor" : [ 1.0, 1.0, 1.0, 1.0 ],
					"clicktabcolor" : [ 0.64323, 0.981721, 0.603215, 0.8 ],
					"fadetime" : 250.0,
					"fadeunselect" : 1,
					"fontface" : 1,
					"fontname" : "Arial",
					"fontsize" : 16.0,
					"hovertabcolor" : [ 0.380502, 0.716542, 0.301725, 0.8 ],
					"htabcolor" : [ 0.25681, 0.47139, 0.6, 1.0 ],
					"htextcolor" : [ 0.075769, 0.075769, 0.075769, 1.0 ],
					"id" : "obj-1",
					"maxclass" : "tab",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1019.372681, 662.908936, 661.0, 29.811157 ],
					"presentation" : 1,
					"presentation_rect" : [ 1.0, -0.300781, 123.0, 117.0 ],
					"rounded" : 0.0,
					"tabcolor" : [ 0.106339, 0.106339, 0.106339, 1.0 ],
					"tabs" : [ "LIGHT", "SOUND", "DEVICES", "CONTROL" ],
					"textcolor" : [ 0.891665, 0.891665, 0.891665, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 11.595187,
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1019.372681, 736.743225, 69.0, 19.0 ],
					"save" : [ "#N", "thispatcher", ";", "#Q", "end", ";" ],
					"text" : "thispatcher",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "bpatcher",
					"name" : "datamanager_modul.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 660.0, 944.380188, 121.0, 351.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 1.0, 116.699219, 121.0, 351.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "bpatcher",
					"name" : "chain_module.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 1019.372681, 1177.0, 510.633911, 292.101196 ],
					"presentation" : 1,
					"presentation_rect" : [ 124.0, 643.0, 510.633911, 292.101196 ],
					"varname" : "chain_modul"
				}

			}
, 			{
				"box" : 				{
					"hidden" : 1,
					"id" : "obj-34",
					"maxclass" : "bpatcher",
					"name" : "sound_modul.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 0.0, 944.380188, 660.0, 405.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 124.0, -0.300781, 660.0, 405.0 ],
					"varname" : "sound_modul"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "bpatcher",
					"name" : "control_modul.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 1007.801636, -2.0, 847.513, 650.457947 ],
					"presentation" : 1,
					"presentation_rect" : [ 124.0, -0.300781, 847.513, 650.457947 ],
					"varname" : "control_modul"
				}

			}
, 			{
				"box" : 				{
					"hidden" : 1,
					"id" : "obj-31",
					"maxclass" : "bpatcher",
					"name" : "light_modul.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 0.0, -2.0, 1007.801636, 946.380188 ],
					"presentation" : 1,
					"presentation_rect" : [ 124.0, -0.300781, 1007.801636, 946.380188 ],
					"varname" : "light_modul"
				}

			}
, 			{
				"box" : 				{
					"hidden" : 1,
					"id" : "obj-9",
					"maxclass" : "bpatcher",
					"name" : "device_modul.maxpat",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 1706.0, 663.908936, 1288.098633, 622.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 124.0, -0.300781, 1288.098633, 622.0 ],
					"varname" : "devices_modul"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-19", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1127.5, 989.0 ],
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-36", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-43", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-46", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-7", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-9::obj-44::obj-135" : [ "textedit[10]", "textedit", 0 ],
			"obj-9::obj-44::obj-132" : [ "textedit[9]", "textedit", 0 ],
			"obj-37::obj-96" : [ "textedit[4]", "textedit", 0 ],
			"obj-9::obj-44::obj-50" : [ "textedit[7]", "textedit", 0 ],
			"obj-9::obj-44::obj-87" : [ "textedit[8]", "textedit", 0 ],
			"obj-37::obj-48" : [ "textedit[3]", "textedit", 0 ],
			"obj-9::obj-44::obj-43" : [ "textedit[6]", "textedit", 0 ],
			"obj-9::obj-44::obj-34" : [ "textedit[5]", "textedit", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "device_modul.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "device_object.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "COMPUTER.txt",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern/DATA",
				"patcherrelativepath" : "./DATA",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "rbtnk.autoBpatcher.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/Objects/externals",
				"patcherrelativepath" : "../Objects/externals",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "rbtnk.autoBpatcher.js",
				"bootpath" : "/Users/Massimo/Documents/Max/Objects/externals",
				"patcherrelativepath" : "../Objects/externals",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "bombeXP_02.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "MachinaMessenger.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "TelUhrNum(Getrennt)_test.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "bombe_Yun_Modul.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "light_modul.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "LIGHT_MOOD.txt",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern/DATA",
				"patcherrelativepath" : "./DATA",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "LIGHT_EFFECT.txt",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern/DATA",
				"patcherrelativepath" : "./DATA",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "LIGHT_FADE.txt",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern/DATA",
				"patcherrelativepath" : "./DATA",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "dmxtoosc_modul.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "dmx_slider_new_4.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "dmx_slider_new.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "control_modul.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "CUE.txt",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern/DATA",
				"patcherrelativepath" : "./DATA",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "phi.lpad.in.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/Objects/externals",
				"patcherrelativepath" : "../Objects/externals",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "phi.lpad.out.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/Objects/externals",
				"patcherrelativepath" : "../Objects/externals",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "phi.patching.router.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/Objects/externals/lib",
				"patcherrelativepath" : "../Objects/externals/lib",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "phi.lpad.palette_mod.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "lpd_colors.txt",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern/DATA",
				"patcherrelativepath" : "./DATA",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "sound_modul.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "MIDIIN.txt",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern/DATA",
				"patcherrelativepath" : "./DATA",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "MIDIOUT.txt",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern/DATA",
				"patcherrelativepath" : "./DATA",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "chain_module.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "EVENTCHAIN.txt",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern/DATA",
				"patcherrelativepath" : "./DATA",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "datamanager_modul.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern",
				"patcherrelativepath" : ".",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "OSC-route.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "OpenSoundControl.mxo",
				"type" : "iLaX"
			}
 ]
	}

}
