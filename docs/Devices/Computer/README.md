Computer
--------

> Was du hier findest ist ein Processing sketch der mit der ursprünglichen Version von 15000 Gray kompatibel ist, leider nicht mit der aktuellen. Wir arbeiten daran! Wenn du dich ein wenig mit der Processing IDE auskennst kannst du natürlich direkt loslegen und die 15000 Gray Computer Konsole auf deinem Rechner zum laufen bringen.

Hier geht es nur um Code, dafür besonders viel. Es gibt viel Platz für zusätzliche Inhalte und Variation. Denk nur daran, dass die wenigsten Spieler deines Games die Sachen entdecken werden die du hier versteckst. Die wenigen eifrigen werden sich dafür umso mehr darüber freuen.

###Was du brauchst

* Processing
* oscP5