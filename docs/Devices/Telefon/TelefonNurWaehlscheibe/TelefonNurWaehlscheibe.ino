/*
*  Waehlscheibe
 *  
 *  Die waehlscheibe ist am braunen und weissen draht zu verbinden. Die polaritaet
 *  ist nicht relevant.
 *  
 *************************************/

int dialPin = 2; // Waehlscheibe auf DI 8
int dialVal = 0;
int dialValStat = 0;

int nummer = 0;
int taste = 10;


unsigned long int signalstart = 0;
unsigned long int signalstop = 0;

unsigned long int signalzeit = 0;
unsigned long int keinsignalzeit = 0;

unsigned long int zwischenzeit = 100;

boolean dialing = false;


void setup()
{
  Serial.begin(115200);  //Initialisierung des USB Ports
  
  Serial.print("arduino ");
  Serial.print("Telefon 05.04.2011");
  Serial.println();
  
  pinMode(dialPin, INPUT);
}

void loop()
{

  dialVal = digitalRead(dialPin);

  /*
  *  Wahlvorgang
   */

  if ( dialVal != dialValStat )
  {
    Serial.print("DIAL ");
    Serial.println(dialVal);
    
    dialValStat = dialVal;

    // bei hochziehen der Waehlscheibe kommt einmalig ein Signal
    if (dialVal == HIGH)
    {
      dialing = true;
      Serial.println("DIALING");
    } 

    else if (dialVal == LOW)
    {
      signalstop = millis();

      //if (dialing)
      //{
      nummer++;

      Serial.print("telefon ");
      Serial.print("klackern");
      Serial.println();

      //udp.sendStr("/Dial", "klacker");
      //}
    }
  }

  /*
  *   Abfrage ob waehlscheibe runtergelaufen
   */

  if ( dialing == true )
  {

    keinsignalzeit = millis() - signalstop;

    if ( keinsignalzeit > zwischenzeit )
    {
      if (nummer == 10) 
      {
        taste = 0;
      } 
      else if (nummer > 0) {
        taste = nummer;
      }
      if (taste < 10) 
      {
        Serial.print("telefon ");
        Serial.print(taste);
        Serial.println();

        //udp.sendInt("/Dial", taste);
      }

      dialing = false;
      nummer = 0;
      taste = 10;
    }
  }
}
