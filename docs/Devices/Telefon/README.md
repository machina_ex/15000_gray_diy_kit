Telefon
=======

Hier gibt es kaum bis gar keine Lötarbeit. Dafür ein besonderes Schmankerl in Punkto Signale und Timing beim Coden. Es ist aber nicht notwendig genau zu verstehen was im Code im Detail passiert.

> Wie so vieles in diesem DIY Kit ist auch diese Anleitung leider noch nicht da. Sorry!!
>ABER! Philip hat in unserer Reihe [Game On Stage](http://machinaex.de/project/game-on-stage/) ein Tutorial verfasst das dir mit Sicherheit weiterhelfen wird:
>[Bitte hier entlang!](https://youtu.be/KcvgQ9eWKDU)
> Völlig unkuratiert haben wir hier die verschiedenen Telefon Codes die über die Jahre entstanden sind (und davon die stabileren) hochgeladen. Schau dich in ruhe um!