{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 6,
			"minor" : 1,
			"revision" : 6,
			"architecture" : "x86"
		}
,
		"rect" : [ 49.0, 70.0, 783.0, 934.0 ],
		"bgcolor" : [ 0.227451, 0.227451, 0.227451, 1.0 ],
		"bglocked" : 1,
		"openinpresentation" : 1,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 0,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 0,
		"statusbarvisible" : 0,
		"toolbarvisible" : 1,
		"boxanimatetime" : 200,
		"imprint" : 0,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"boxes" : [ 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-112",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1149.5, 443.538513, 60.0, 20.0 ],
					"text" : "s EVENT",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-149",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 1232.0, 286.716492, 74.0, 20.0 ],
					"text" : "sel pongwin",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-145",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 3,
					"outlettype" : [ "bang", "bang", "" ],
					"patching_rect" : [ 1232.0, 196.23761, 125.0, 20.0 ],
					"text" : "sel tetrislose tetriswin",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-124",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1122.5, 240.0, 74.0, 20.0 ],
					"text" : "prepend set",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-117",
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1136.0, 311.716492, 185.496338, 52.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 295.013794, 40.77124, 287.767273, 52.0 ],
					"text" : "D:\\\\system\\\\results>",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-92",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1064.36731, 372.697235, 35.0, 18.0 ],
					"text" : "suck",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-91",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1019.79541, 430.227966, 90.0, 20.0 ],
					"text" : "s computerudp",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-90",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1024.0, 912.0, 88.0, 20.0 ],
					"text" : "r computerudp",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-89",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1096.86731, 286.716492, 32.5, 18.0 ],
					"text" : "quit",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-83",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1019.79541, 405.420471, 41.0, 18.0 ],
					"text" : "ronny",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-82",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1064.36731, 405.420471, 40.0, 18.0 ],
					"text" : "paula",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-81",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1019.79541, 372.697235, 37.0, 18.0 ],
					"text" : "nuke",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-80",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1019.79541, 319.716492, 60.0, 18.0 ],
					"text" : "tetrisboot",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-74",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1019.79541, 337.716492, 61.0, 18.0 ],
					"text" : "tetrissuck",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-71",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1053.79541, 286.716492, 33.0, 18.0 ],
					"text" : "stop",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-66",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1149.5, 415.88324, 50.0, 18.0 ],
					"text" : "PAULA",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-46",
					"linecount" : 3,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1151.496338, 205.833496, 50.0, 45.0 ],
					"text" : "D:systemresults>",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-44",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 1136.0, 167.77124, 197.0, 20.0 ],
					"text" : "route /textinput /trigger /tetris /pong",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-59",
					"linecount" : 5,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 638.568237, 1211.0, 150.0, 74.0 ],
					"text" : "das sollte eigentlich geschickt werden, aber ich hab es umgebaut auf reine wortbefehle ohne OSC",
					"textcolor" : [ 0.63626, 0.615254, 0.513134, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-33",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1019.79541, 286.716492, 34.0, 18.0 ],
					"text" : "start",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 588.5, 665.703979, 33.0, 20.0 ],
					"text" : "t b s",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-9",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 653.00647, 523.703979, 34.0, 20.0 ],
					"text" : "gate",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 508.5, 603.703979, 68.0, 20.0 ],
					"text" : "route bang",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 508.5, 573.703979, 61.0, 20.0 ],
					"text" : "route text",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-27",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 568.75, 515.703979, 32.5, 18.0 ],
					"text" : "2",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-32",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 537.0, 515.703979, 32.5, 18.0 ],
					"text" : "1",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-21",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 537.0, 545.703979, 44.0, 20.0 ],
					"text" : "gate 2",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-24",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 471.5, 634.703979, 105.0, 20.0 ],
					"text" : "join 2 @triggers 0",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-26",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 471.5, 665.703979, 99.0, 20.0 ],
					"text" : "prepend subsym",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-52",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 537.0, 483.703979, 36.0, 20.0 ],
					"text" : "sel 0",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-3",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 129.191467, 84.77845, 150.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 130.691498, 84.77845, 97.0, 20.0 ],
					"text" : "OSC_RECIEVE",
					"textcolor" : [ 0.63626, 0.615254, 0.513134, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-2",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 207.0, 730.0, 150.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 19.19146, 84.77845, 76.0, 20.0 ],
					"text" : "OSC_SEND",
					"textcolor" : [ 0.63626, 0.615254, 0.513134, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-152",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1016.996338, 196.23761, 73.0, 20.0 ],
					"text" : "fromsymbol",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"comment" : "RAUMWAHL",
					"id" : "obj-14",
					"maxclass" : "inlet",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1224.496338, -1.414612, 25.0, 25.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-172",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 392.0, 261.37915, 64.0, 20.0 ],
					"text" : "sel NONE",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-110",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 37.172577, 169.77124, 33.0, 20.0 ],
					"text" : "t b s",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ],
					"varname" : "cuetrigger_1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-105",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ -1.643066, 204.902222, 81.0, 20.0 ],
					"text" : "prepend goto",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ],
					"varname" : "gotocue_1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-38",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 330.0, 261.37915, 49.0, 20.0 ],
					"text" : "delay 5",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-30",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 381.0, 291.551575, 32.5, 18.0 ],
					"text" : "set",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-17",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 313.060974, 291.551575, 47.0, 18.0 ],
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-28",
					"maxclass" : "newobj",
					"numinlets" : 3,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 342.0, 359.038513, 46.0, 20.0 ],
					"text" : "join 3",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-31",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 356.0, 204.902222, 33.0, 18.0 ],
					"text" : "next",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ],
					"varname" : "nextcue_1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-25",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 313.060974, 317.716492, 61.0, 20.0 ],
					"text" : "sel NEXT",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-163",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 784.197754, 1005.742432, 33.0, 20.0 ],
					"text" : "t b s",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-141",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 1002.803711, 1079.807251, 120.0, 20.0 ],
					"restore" : [ "192.168.2.8" ],
					"saved_object_attributes" : 					{
						"parameter_enable" : 0
					}
,
					"text" : "pattr #device_port_1",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ],
					"varname" : "#device_port_1[1]"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-137",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 1149.200928, 1114.807251, 120.0, 20.0 ],
					"restore" : [ 12000 ],
					"saved_object_attributes" : 					{
						"parameter_enable" : 0
					}
,
					"text" : "pattr #device_port_1",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ],
					"varname" : "#device_port_1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-133",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1149.200928, 1077.012207, 68.0, 20.0 ],
					"text" : "route bang",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-134",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1149.200928, 1052.690186, 61.0, 20.0 ],
					"text" : "route text",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bordercolor" : [ 1.0, 0.968484, 0.807154, 1.0 ],
					"clickmode" : 1,
					"fontface" : 1,
					"fontname" : "Ubuntu Medium",
					"fontsize" : 14.0,
					"frgb" : 0.0,
					"id" : "obj-135",
					"keymode" : 1,
					"lines" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1149.200928, 1026.247314, 205.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 475.761993, 104.77845, 68.0, 23.0 ],
					"rounded" : 0.0,
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "textedit[10]",
							"parameter_shortname" : "textedit",
							"parameter_type" : 3,
							"parameter_invisible" : 1
						}

					}
,
					"separator" : "\u002c",
					"tabmode" : 0,
					"text" : "12000",
					"textcolor" : [ 0.213148, 0.523504, 0.563455, 1.0 ],
					"varname" : "device_port_1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-129",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1002.803711, 1036.690186, 68.0, 20.0 ],
					"text" : "route bang",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-130",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1002.803711, 1012.368164, 61.0, 20.0 ],
					"text" : "route text",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bordercolor" : [ 1.0, 0.968484, 0.807154, 1.0 ],
					"clickmode" : 1,
					"fontface" : 1,
					"fontname" : "Ubuntu Medium",
					"fontsize" : 14.0,
					"frgb" : 0.0,
					"id" : "obj-132",
					"keymode" : 1,
					"lines" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1002.803711, 985.925293, 205.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 379.761963, 104.77845, 98.0, 23.0 ],
					"rounded" : 0.0,
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "textedit[9]",
							"parameter_shortname" : "textedit",
							"parameter_type" : 3,
							"parameter_invisible" : 1
						}

					}
,
					"separator" : "\u002c",
					"tabmode" : 0,
					"text" : "192.168.2.8",
					"textcolor" : [ 0.213148, 0.523504, 0.563455, 1.0 ],
					"varname" : "device_ip_1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-29",
					"linecount" : 4,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 572.330566, 1044.37854, 87.802917, 58.0 ],
					"text" : "/COMPUTER/SUBADRESS/MESSAGE STATE",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-102",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "bang" ],
					"patching_rect" : [ 752.568237, 1102.542358, 34.0, 20.0 ],
					"text" : "t b b",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 9.0,
					"id" : "obj-94",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "OSCTimeTag" ],
					"patching_rect" : [ 638.568237, 1143.7323, 88.0, 17.0 ],
					"text" : "OpenSoundControl",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Ubuntu Medium",
					"fontsize" : 12.0,
					"id" : "obj-101",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 784.197754, 962.689819, 113.0, 21.0 ],
					"text" : "join 2 @triggers 1",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 14.0,
					"id" : "obj-104",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 904.568237, 1079.807251, 77.0, 22.0 ],
					"text" : "r loadbang",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.450051, 0.354526, 0.09558, 0.0 ],
					"bubble" : 1,
					"fontface" : 1,
					"fontname" : "Arial",
					"fontsize" : 14.0,
					"frgb" : 0.0,
					"id" : "obj-107",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 912.068237, 1289.7323, 176.0, 42.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 19.19146, 12.174438, 580.5896, 26.0 ],
					"text" : "/COMPUTER/SUBADRESS/MESSAGE STATE",
					"textcolor" : [ 0.117391, 0.117391, 0.117391, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 14.0,
					"id" : "obj-108",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 912.068237, 1259.7323, 84.0, 22.0 ],
					"text" : "prepend set",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 14.0,
					"id" : "obj-118",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1085.568237, 1149.7323, 90.0, 22.0 ],
					"text" : "prepend port",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 14.0,
					"id" : "obj-119",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 979.068237, 1149.7323, 92.0, 22.0 ],
					"text" : "prepend host",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 14.0,
					"id" : "obj-126",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 969.00293, 1206.026245, 208.0, 22.0 ],
					"text" : "udpsend 192.168.178.30 12000",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 14.0,
					"id" : "obj-128",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 904.568237, 1114.807251, 32.5, 20.0 ],
					"text" : "set",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Ubuntu Medium",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-106",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 841.728394, 1289.7323, 48.0, 21.0 ],
					"text" : "SENT",
					"textcolor" : [ 0.911579, 0.922673, 0.650671, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-103",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 8,
					"outlettype" : [ "bang", "bang", "bang", "bang", "bang", "bang", "bang", "" ],
					"patching_rect" : [ 138.5, 326.716492, 113.5, 20.0 ],
					"text" : "t b b b b b b b s",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-100",
					"linecount" : 2,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 695.0, 857.0, 50.0, 31.0 ],
					"text" : "MESSAGE",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-95",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 578.5, 862.0, 84.0, 18.0 ],
					"text" : "SUBADRESS",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-53",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 179.0, 965.0, 260.0, 18.0 ],
					"text" : "/COMPUTER/SUBADRESS/MESSAGE",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-96",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 458.0, 965.0, 50.0, 18.0 ],
					"text" : "STATE",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-93",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 5,
					"outlettype" : [ "", "", "", "", "" ],
					"patching_rect" : [ 205.668945, 274.085205, 73.0, 20.0 ],
					"text" : "unjoin 4",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ],
					"varname" : "route_1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-88",
					"maxclass" : "newobj",
					"numinlets" : 5,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 416.5, 889.646973, 381.0, 20.0 ],
					"text" : "combine OSCADRESS / SUBADRESS1 / SUBADRESS2 @triggers 2",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-97",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1016.996338, 571.868896, 90.0, 20.0 ],
					"text" : "pattrhub object",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ],
					"varname" : "object"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-60",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 498.5, 832.117004, 113.0, 20.0 ],
					"restore" : [ "/COMPUTER" ],
					"saved_object_attributes" : 					{
						"parameter_enable" : 0
					}
,
					"text" : "pattr #oscadress_1",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ],
					"varname" : "#oscadress_1[1]"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-39",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 652.0, 813.794983, 129.0, 20.0 ],
					"restore" : [ "Computer" ],
					"saved_object_attributes" : 					{
						"parameter_enable" : 0
					}
,
					"text" : "pattr #device_name_1",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ],
					"varname" : "#device_name_1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-36",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 104.0, 492.511475, 83.0, 20.0 ],
					"text" : "prepend refer",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-22",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 951.988037, 205.833496, 32.5, 18.0 ],
					"text" : "0",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-20",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 916.738037, 205.833496, 32.5, 18.0 ],
					"text" : "1",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "led",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 951.988037, 251.038513, 20.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 168.191498, 55.77124, 22.0, 22.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-13",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 1016.996338, 545.703979, 112.0, 20.0 ],
					"text" : "rbtnk.autoBpatcher",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-85",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 299.0, 74.850403, 68.0, 20.0 ],
					"text" : "route bang",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-86",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 299.0, 50.528381, 61.0, 20.0 ],
					"text" : "route text",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bordercolor" : [ 1.0, 0.968484, 0.807154, 1.0 ],
					"clickmode" : 1,
					"fontface" : 1,
					"fontname" : "Ubuntu Medium",
					"fontsize" : 14.0,
					"frgb" : 0.0,
					"id" : "obj-87",
					"keymode" : 1,
					"lines" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 299.0, 22.349335, 205.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 19.19146, 127.77845, 251.417236, 23.0 ],
					"rounded" : 0.0,
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "textedit[8]",
							"parameter_shortname" : "textedit",
							"parameter_type" : 3,
							"parameter_invisible" : 1
						}

					}
,
					"separator" : "\u002c",
					"tabmode" : 0,
					"text" : "Computer.txt",
					"textcolor" : [ 0.213148, 0.523504, 0.563455, 1.0 ],
					"varname" : "datafile_eventhandling_1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-84",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 865.738037, 72.038513, 20.0, 20.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-73",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 916.738037, 115.038513, 69.0, 20.0 ],
					"text" : "delay 1000",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-67",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 811.738037, 169.77124, 181.0, 18.0 ],
					"text" : "0.210312 0.210312 0.210312 1.",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-63",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 716.238037, 128.038513, 181.0, 18.0 ],
					"text" : "0.772812 0.772812 0.772812 1.",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-57",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 811.738037, 205.833496, 97.0, 20.0 ],
					"text" : "prepend bgcolor",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-127",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 634.426636, 196.23761, 90.0, 20.0 ],
					"text" : "prepend OUT_",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-125",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 557.426636, 196.23761, 77.0, 20.0 ],
					"text" : "prepend IN_",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-123",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 439.678467, 196.23761, 109.0, 20.0 ],
					"text" : "join 2 @triggers -1",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-122",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 439.678467, 228.53363, 74.0, 20.0 ],
					"text" : "prepend set",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-121",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 548.678467, 251.038513, 151.0, 20.0 ],
					"text" : "prepend locationandobject",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-113",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1023.496338, 226.88324, 60.0, 20.0 ],
					"text" : "s EVENT",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-70",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 990.0, -1.414612, 231.996368, 20.0 ],
					"text" : "Network_Device > Internal_Events",
					"textcolor" : [ 0.63626, 0.615254, 0.513134, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-69",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 419.0, 750.0, 231.996368, 20.0 ],
					"text" : "Internal_Events > Network_Device",
					"textcolor" : [ 0.63626, 0.615254, 0.513134, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-68",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 1016.996338, 262.37915, 231.996368, 20.0 ],
					"text" : "Internal_Events > Global_Events",
					"textcolor" : [ 0.63626, 0.615254, 0.513134, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-61",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 13.672577, 11.075104, 231.996368, 20.0 ],
					"text" : "Globale_Events > Internal_Events",
					"textcolor" : [ 0.63626, 0.615254, 0.513134, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-23",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1016.996338, 82.792328, 68.0, 20.0 ],
					"text" : "route bang",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1016.996338, 54.792328, 61.0, 20.0 ],
					"text" : "route text",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bordercolor" : [ 1.0, 0.968484, 0.807154, 1.0 ],
					"clickmode" : 1,
					"fontface" : 1,
					"fontname" : "Ubuntu Medium",
					"fontsize" : 14.0,
					"frgb" : 0.0,
					"id" : "obj-50",
					"keymode" : 1,
					"lines" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 1016.996338, 28.349335, 205.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 130.691498, 104.77845, 139.917206, 23.0 ],
					"rounded" : 0.0,
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "textedit[7]",
							"parameter_shortname" : "textedit",
							"parameter_type" : 3,
							"parameter_invisible" : 1
						}

					}
,
					"separator" : "\u002c",
					"tabmode" : 0,
					"text" : "/COMPUTER",
					"textcolor" : [ 0.213148, 0.523504, 0.563455, 1.0 ],
					"varname" : "edit_events_intern[4]"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-12",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 1016.996338, 167.77124, 109.0, 20.0 ],
					"text" : "OSC-route /trigger",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-56",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1016.996338, 121.23761, 53.0, 18.0 ],
					"text" : "set 1 $1",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Ubuntu Medium",
					"fontsize" : 14.0,
					"id" : "obj-51",
					"maxclass" : "number",
					"maximum" : 12100,
					"minimum" : 12000,
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "int", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 1095.496338, 53.792328, 83.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-49",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1095.496338, 82.792328, 79.0, 20.0 ],
					"text" : "prepend port",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-48",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 1095.496338, 121.23761, 106.0, 20.0 ],
					"text" : "udpreceive 12001",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Ubuntu Light",
					"fontsize" : 14.0,
					"frgb" : 0.0,
					"id" : "obj-47",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 785.238037, 710.0, 150.0, 23.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 379.761963, 127.77845, 164.000031, 23.0 ],
					"text" : "DEVICE",
					"textcolor" : [ 0.911579, 0.922673, 0.650671, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-41",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 707.0, 786.939026, 68.0, 20.0 ],
					"text" : "route bang",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-42",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 707.0, 762.617004, 61.0, 20.0 ],
					"text" : "route text",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bordercolor" : [ 1.0, 0.968484, 0.807154, 1.0 ],
					"clickmode" : 1,
					"fontface" : 1,
					"fontname" : "Ubuntu Medium",
					"fontsize" : 14.0,
					"frgb" : 0.0,
					"id" : "obj-43",
					"keymode" : 1,
					"lines" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 707.0, 736.174011, 205.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 379.761963, 150.778442, 192.0, 23.0 ],
					"rounded" : 0.0,
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "textedit[6]",
							"parameter_shortname" : "textedit",
							"parameter_type" : 3,
							"parameter_invisible" : 1
						}

					}
,
					"separator" : "\u002c",
					"tabmode" : 0,
					"text" : "Computer",
					"textcolor" : [ 0.213148, 0.523504, 0.563455, 1.0 ],
					"varname" : "device_name_1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-37",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 419.0, 851.117004, 68.0, 20.0 ],
					"text" : "route bang",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-35",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 419.0, 826.794983, 61.0, 20.0 ],
					"text" : "route text",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bordercolor" : [ 1.0, 0.968484, 0.807154, 1.0 ],
					"clickmode" : 1,
					"fontface" : 1,
					"fontname" : "Ubuntu Medium",
					"fontsize" : 14.0,
					"frgb" : 0.0,
					"id" : "obj-34",
					"keymode" : 1,
					"lines" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 1,
					"patching_rect" : [ 419.0, 800.35199, 205.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 19.19146, 104.77845, 111.500031, 23.0 ],
					"rounded" : 0.0,
					"saved_attribute_attributes" : 					{
						"valueof" : 						{
							"parameter_longname" : "textedit[5]",
							"parameter_shortname" : "textedit",
							"parameter_type" : 3,
							"parameter_invisible" : 1
						}

					}
,
					"separator" : "\u002c",
					"tabmode" : 0,
					"text" : "/COMPUTER",
					"textcolor" : [ 0.213148, 0.523504, 0.563455, 1.0 ],
					"varname" : "oscadress_1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "gswitch2",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 13.672577, 687.0, 39.0, 32.0 ],
					"varname" : "selectgate_1"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontname" : "Arial",
					"fontsize" : 14.0,
					"frgb" : 0.0,
					"id" : "obj-242",
					"linecount" : 2,
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 381.0, 425.538513, 64.0, 38.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 259.31073, 191.139694, 71.961334, 22.0 ],
					"text" : "Edit Field",
					"textcolor" : [ 0.911579, 0.922673, 0.650671, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontname" : "Arial",
					"fontsize" : 14.0,
					"frgb" : 0.0,
					"id" : "obj-8",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 5.172577, 139.684143, 64.0, 22.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 19.572639, 150.778442, 92.0, 22.0 ],
					"text" : "File",
					"textcolor" : [ 0.911579, 0.922673, 0.650671, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Ubuntu Medium",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-5",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 221.668945, 116.510193, 54.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 276.291382, 157.778442, 38.0, 21.0 ],
					"text" : "SAVE",
					"textcolor" : [ 0.911579, 0.922673, 0.650671, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Ubuntu Medium",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-4",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 299.0, 116.510193, 57.0, 21.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 314.672577, 157.778442, 56.0, 21.0 ],
					"text" : "SAVE AS",
					"textcolor" : [ 0.911579, 0.922673, 0.650671, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-111",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 13.672577, 40.585388, 58.0, 20.0 ],
					"text" : "r EVENT",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-75",
					"maxclass" : "gswitch2",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 37.172577, 274.085205, 39.0, 32.0 ],
					"varname" : "editgate_1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 14.0,
					"id" : "obj-62",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 104.0, 400.0, 32.5, 20.0 ],
					"text" : "0",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-58",
					"maxclass" : "gswitch2",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 13.672577, 527.0, 39.0, 32.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-77",
					"maxclass" : "toggle",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 16.856934, 376.0, 20.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 353.756348, 185.101196, 28.038498, 28.038498 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-78",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "select", "" ],
					"patching_rect" : [ 313.060974, 789.0, 59.0, 21.0 ],
					"text" : "t select l",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontname" : "Arial",
					"fontsize" : 16.0,
					"frgb" : 0.0,
					"id" : "obj-79",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 44.856934, 374.0, 203.0, 24.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 387.279144, 189.139694, 128.354767, 24.0 ],
					"text" : "EDIT ON/OFF",
					"textcolor" : [ 0.911579, 0.922673, 0.650671, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-148",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 63.0, 439.0, 67.0, 20.0 ],
					"text" : "r loadbang",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-195",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 104.0, 760.286926, 59.5, 20.0 ],
					"text" : "unjoin 3",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-191",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 104.0, 472.511475, 49.0, 18.0 ],
					"text" : "Bombe",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"bblend" : 50,
					"bgcolor" : [ 0.223529, 0.223529, 0.223529, 0.85 ],
					"bordercolor" : [ 0.840014, 0.840014, 0.840014, 1.0 ],
					"colhead" : 1,
					"cols" : 3,
					"colwidth" : 100,
					"fblend" : 50,
					"fgcolor" : [ 0.853991, 0.853991, 0.853991, 1.0 ],
					"fontname" : "Arial",
					"fontsize" : 14.0,
					"gridlinecolor" : [ 0.848879, 0.848879, 0.848879, 1.0 ],
					"hcellcolor" : [ 0.0, 0.515776, 0.650934, 1.0 ],
					"id" : "obj-189",
					"maxclass" : "jit.cellblock",
					"numinlets" : 2,
					"numoutlets" : 4,
					"outlettype" : [ "list", "", "", "" ],
					"outmode" : 1,
					"patching_rect" : [ 104.0, 518.703979, 286.999969, 200.29599 ],
					"presentation" : 1,
					"presentation_rect" : [ 19.572639, 220.663666, 498.061279, 108.437531 ],
					"rowhead" : 1,
					"rows" : 5,
					"sccolor" : [ 0.252191, 0.252191, 0.252191, 1.0 ],
					"sgcolor" : [ 0.513186, 0.512421, 0.525781, 1.0 ],
					"stcolor" : [ 0.435686, 0.642805, 0.901834, 1.0 ],
					"textcolor" : [ 1.0, 1.0, 1.0, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"autoscroll" : 0,
					"bangmode" : 1,
					"bgcolor" : [ 0.185424, 0.185458, 0.185415, 1.0 ],
					"bordercolor" : [ 0.883679, 0.883679, 0.883679, 1.0 ],
					"clickmode" : 1,
					"fontface" : 1,
					"fontname" : "Arial",
					"fontsize" : 16.0,
					"frgb" : 0.0,
					"id" : "obj-216",
					"keymode" : 1,
					"lines" : 1,
					"maxclass" : "textedit",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 134.168945, 434.980743, 239.0, 28.038502 ],
					"presentation" : 1,
					"presentation_rect" : [ 19.572639, 185.101196, 235.253784, 28.038502 ],
					"textcolor" : [ 0.778684, 0.778831, 0.778647, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 14.0,
					"id" : "obj-65",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 96.356934, 72.585388, 37.0, 22.0 ],
					"text" : "t b s",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"frgb" : 0.0,
					"id" : "obj-162",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 299.0, 145.030396, 90.0, 20.0 ],
					"text" : "Data Save File",
					"textcolor" : [ 0.63626, 0.615254, 0.513134, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-139",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 205.668945, 94.850403, 96.0, 20.0 ],
					"text" : "prepend symbol",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-212",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 96.356934, 40.585388, 71.0, 20.0 ],
					"text" : "r datafolder",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-54",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 96.356934, 106.684143, 95.0, 21.0 ],
					"text" : "prepend prefix",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"autopopulate" : 1,
					"bgcolor" : [ 0.118823, 0.437276, 0.599171, 1.0 ],
					"fontface" : 1,
					"fontname" : "Arial",
					"fontsize" : 16.0,
					"framecolor" : [ 0.430632, 0.430713, 0.430611, 1.0 ],
					"id" : "obj-210",
					"items" : [ "COMPUTER.txt", ",", "CUE.txt", ",", "EVENTCHAIN.txt", ",", "launchpad_new.png", ",", "LIGHT_CUE.txt", ",", "LIGHT_EFFECT.txt", ",", "LIGHT_FADE.txt", ",", "LIGHT_MOOD.txt", ",", "lpd_colors.txt", ",", "MIDIIN.txt", ",", "MIDIOUT.txt" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 96.356934, 145.030396, 100.0, 24.0 ],
					"prefix" : " Macintosh HD:/Users/Limor/Documents/Max/15kGray_Engine_Luzern/DATA/",
					"presentation" : 1,
					"presentation_rect" : [ 19.19146, 55.77124, 144.0, 24.0 ],
					"textcolor" : [ 0.820861, 0.821015, 0.820822, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-207",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 96.356934, 189.190735, 82.0, 20.0 ],
					"text" : "prepend read",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ],
					"varname" : "read_1"
				}

			}
, 			{
				"box" : 				{
					"blinkcolor" : [ 0.494503, 0.640406, 0.866869, 1.0 ],
					"fgcolor" : [ 0.254902, 0.294118, 0.352941, 1.0 ],
					"id" : "obj-164",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"outlinecolor" : [ 0.995264, 0.995353, 0.821557, 1.0 ],
					"patching_rect" : [ 225.668945, 145.030396, 20.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 314.291382, 127.77845, 23.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-165",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"outlinecolor" : [ 1.0, 0.987462, 0.849302, 1.0 ],
					"patching_rect" : [ 272.0, 145.030396, 20.0, 20.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 276.291382, 127.77845, 23.0, 23.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-220",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 299.0, 173.73645, 56.0, 20.0 ],
					"text" : "r saveas",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-218",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 225.668945, 173.73645, 43.0, 20.0 ],
					"text" : "r save",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-142",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 272.0, 204.902222, 71.0, 19.0 ],
					"text" : "writeagain",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ],
					"varname" : "writeagain_1"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 13.0,
					"id" : "obj-55",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 225.668945, 204.902222, 39.0, 19.0 ],
					"text" : "write",
					"textcolor" : [ 0.105882, 0.113725, 0.117647, 1.0 ],
					"varname" : "write_1"
				}

			}
, 			{
				"box" : 				{
					"bgcolor" : [ 0.210312, 0.210312, 0.210312, 1.0 ],
					"border" : 5,
					"bordercolor" : [ 1.0, 1.0, 1.0, 0.5 ],
					"id" : "obj-19",
					"maxclass" : "panel",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 811.738037, 251.038513, 128.0, 128.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 3.486259, -2.721558, 612.0, 341.0 ],
					"rounded" : 0
				}

			}
, 			{
				"box" : 				{
					"background" : 1,
					"bgcolor" : [ 0.194412, 0.194412, 0.194412, 0.83 ],
					"bordercolor" : [ 0.904348, 0.904348, 0.904348, 1.0 ],
					"id" : "obj-64",
					"maxclass" : "panel",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ -1.643066, -1.414612, 991.643066, 1045.414551 ]
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Arial",
					"fontsize" : 12.0,
					"id" : "obj-11",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 100.0, 240.0, 70.0, 20.0 ],
					"saved_object_attributes" : 					{
						"embed" : 0
					}
,
					"text" : "coll Bombe",
					"textcolor" : [ 0.65098, 0.886275, 0.180392, 1.0 ],
					"varname" : "object_1"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-10", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 4 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-100", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-108", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-163", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-101", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-102", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-102", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-103", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-103", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-103", 6 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-103", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-103", 7 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-95", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-103", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-103", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-128", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-104", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-105", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-108", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-93", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-11", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-105", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-110", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-110", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-110", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-111", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-118", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-119", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-152", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-12", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-84", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 558.178467, 279.0, 534.0, 279.0, 534.0, 228.0, 798.0, 228.0, 798.0, 138.0, 852.0, 138.0, 852.0, 69.0, 875.238037, 69.0 ],
					"source" : [ "obj-121", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 449.178467, 258.0, 534.0, 258.0, 534.0, 246.0, 558.178467, 246.0 ],
					"source" : [ "obj-122", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-122", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 449.178467, 216.0, 449.178467, 216.0 ],
					"source" : [ "obj-123", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-117", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-124", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 566.926636, 246.0, 558.178467, 246.0 ],
					"source" : [ "obj-125", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-121", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 643.926636, 246.0, 558.178467, 246.0 ],
					"source" : [ "obj-127", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-107", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-128", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-119", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-129", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-141", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-129", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-129", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1012.303711, 1031.573242, 1012.303711, 1031.573242 ],
					"source" : [ "obj-130", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-130", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1012.303711, 1007.573303, 1012.303711, 1007.573303 ],
					"source" : [ "obj-132", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-118", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-133", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-137", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-133", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-133", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1158.700928, 1071.895264, 1158.700928, 1071.895264 ],
					"source" : [ "obj-134", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-134", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1158.700928, 1047.895264, 1158.700928, 1047.895264 ],
					"source" : [ "obj-135", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-135", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-137", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-210", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 215.168945, 117.0, 207.0, 117.0, 207.0, 141.0, 105.856934, 141.0 ],
					"source" : [ "obj-139", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-132", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-141", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-142", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-74", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-145", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-80", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-145", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-191", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 72.5, 465.0, 113.5, 465.0 ],
					"source" : [ "obj-148", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 72.5, 459.0, 60.0, 459.0, 60.0, 408.0, 99.0, 408.0, 99.0, 399.0, 113.5, 399.0 ],
					"source" : [ "obj-148", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-149", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-113", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-152", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-110", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-102", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-163", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-163", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-55", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 235.168945, 165.0, 222.0, 165.0, 222.0, 198.0, 235.168945, 198.0 ],
					"source" : [ "obj-164", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 281.5, 165.0, 281.5, 165.0 ],
					"source" : [ "obj-165", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-172", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-18", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-195", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 113.5, 714.0, 113.5, 714.0 ],
					"source" : [ "obj-189", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-78", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 202.833328, 774.0, 322.560974, 774.0 ],
					"source" : [ "obj-189", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-191", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 140.5, 810.0, 210.0, 810.0, 210.0, 723.0, 63.0, 723.0, 63.0, 684.0, 43.172577, 684.0 ],
					"source" : [ "obj-195", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-195", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-195", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-207", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-207", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 146.356934, 171.0, 105.856934, 171.0 ],
					"source" : [ "obj-210", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-65", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 105.856934, 60.0, 105.856934, 60.0 ],
					"source" : [ "obj-212", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 143.668945, 465.0, 90.0, 465.0, 90.0, 513.0, 43.172577, 513.0 ],
					"source" : [ "obj-216", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-216", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-55", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 235.168945, 195.0, 235.168945, 195.0 ],
					"source" : [ "obj-218", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-142", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 308.5, 195.0, 282.0, 195.0, 282.0, 201.0, 281.5, 201.0 ],
					"source" : [ "obj-220", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-56", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1075.496338, 102.0, 1026.496338, 102.0 ],
					"source" : [ "obj-23", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-26", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-25", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-26", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-127", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-94", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-35", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 428.5, 822.0, 428.5, 822.0 ],
					"source" : [ "obj-34", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 428.5, 846.0, 428.5, 846.0 ],
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-189", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-37", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 477.5, 876.381958, 426.0, 876.381958 ],
					"source" : [ "obj-37", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-39", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1026.496338, 75.0, 1026.496338, 75.0 ],
					"source" : [ "obj-40", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-123", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 765.5, 815.939026, 652.339233, 815.939026, 652.339233, 186.23761, 539.178467, 186.23761 ],
					"source" : [ "obj-41", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-41", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-41", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 716.5, 781.822021, 716.5, 781.822021 ],
					"source" : [ "obj-42", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-42", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 716.5, 757.822021, 716.5, 757.822021 ],
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-112", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-44", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-124", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-145", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-44", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-149", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-44", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-46", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-66", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-44", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1104.996338, 147.0, 1029.0, 147.0, 1029.0, 153.0, 1026.496338, 153.0 ],
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-48", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-48", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1104.996338, 102.0, 1104.996338, 102.0 ],
					"source" : [ "obj-49", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1026.496338, 51.0, 1026.496338, 51.0 ],
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-49", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1104.996338, 78.0, 1104.996338, 78.0 ],
					"source" : [ "obj-51", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-52", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-53", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-210", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 105.856934, 129.0, 105.856934, 129.0 ],
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-55", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-12", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 1026.496338, 141.0, 1026.496338, 141.0 ],
					"source" : [ "obj-56", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-19", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 821.238037, 225.0, 821.238037, 225.0 ],
					"source" : [ "obj-57", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-189", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 43.172577, 561.0, 90.0, 561.0, 90.0, 507.0, 113.5, 507.0 ],
					"source" : [ "obj-58", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-34", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-60", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-77", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 113.5, 420.0, 36.0, 420.0, 36.0, 372.0, 26.356934, 372.0 ],
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 725.738037, 180.0, 821.238037, 180.0 ],
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 123.856934, 96.0, 108.0, 96.0, 108.0, 102.0, 105.856934, 102.0 ],
					"source" : [ "obj-65", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-87", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-65", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-57", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 821.238037, 192.0, 821.238037, 192.0 ],
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-7", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-191", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-71", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 926.238037, 135.0, 821.238037, 135.0 ],
					"source" : [ "obj-73", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-74", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-11", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-75", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 26.356934, 513.0, 0.0, 513.0, 0.0, 672.0, 23.172577, 672.0 ],
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-58", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 26.356934, 522.0, 23.172577, 522.0 ],
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-75", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 26.356934, 396.0, 3.0, 396.0, 3.0, 270.0, 46.672577, 270.0 ],
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-9", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-77", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-216", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 362.560974, 810.0, 402.0, 810.0, 402.0, 474.0, 375.0, 474.0, 375.0, 420.0, 143.668945, 420.0 ],
					"source" : [ "obj-78", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-216", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 322.560974, 810.0, 210.0, 810.0, 210.0, 723.0, 63.0, 723.0, 63.0, 471.0, 48.0, 471.0, 48.0, 426.0, 143.668945, 426.0 ],
					"source" : [ "obj-78", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-80", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-81", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-82", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-83", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-84", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 875.238037, 147.0, 725.738037, 147.0 ],
					"source" : [ "obj-84", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-73", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 875.238037, 102.0, 926.238037, 102.0 ],
					"source" : [ "obj-84", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-139", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 357.5, 91.925201, 215.168945, 91.925201 ],
					"source" : [ "obj-85", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-85", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 308.5, 72.0, 308.5, 72.0 ],
					"source" : [ "obj-86", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-86", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 308.5, 45.0, 308.5, 45.0 ],
					"source" : [ "obj-87", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-53", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-88", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-89", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-9", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-126", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-90", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-91", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-92", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-100", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-93", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-103", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-93", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-93", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-172", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-93", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-93", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-93", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-93", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-93", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-88", 2 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-95", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-101", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-96", 0 ]
				}

			}
 ],
		"parameters" : 		{
			"obj-50" : [ "textedit[7]", "textedit", 0 ],
			"obj-135" : [ "textedit[10]", "textedit", 0 ],
			"obj-132" : [ "textedit[9]", "textedit", 0 ],
			"obj-87" : [ "textedit[8]", "textedit", 0 ],
			"obj-43" : [ "textedit[6]", "textedit", 0 ],
			"obj-34" : [ "textedit[5]", "textedit", 0 ]
		}
,
		"dependency_cache" : [ 			{
				"name" : "COMPUTER.txt",
				"bootpath" : "/Users/Massimo/Documents/Max/15kGray_Engine_Luzern/DATA",
				"patcherrelativepath" : "./DATA",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "rbtnk.autoBpatcher.maxpat",
				"bootpath" : "/Users/Massimo/Documents/Max/Objects/externals",
				"patcherrelativepath" : "../Objects/externals",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "rbtnk.autoBpatcher.js",
				"bootpath" : "/Users/Massimo/Documents/Max/Objects/externals",
				"patcherrelativepath" : "../Objects/externals",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "OSC-route.mxo",
				"type" : "iLaX"
			}
, 			{
				"name" : "OpenSoundControl.mxo",
				"type" : "iLaX"
			}
 ]
	}

}
