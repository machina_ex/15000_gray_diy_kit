# 15 000 Gray Computer Pseudo Terminal Electron Version

this is the Electronjs based version of machina eX' 15000 Gray prop "Computer" which displays a computer terminal that resembles a mixture of DOS and Unix inspired commandlines, but does not quite work as expected. So you probably have to find the right notes somewhere in the lab....

It comes with a UDP and a TCP interface, expecting UDP or TCP raw string inputs as follows (a HTTP API is wanted, but for compatibility reasons we focussed on UDP and TCP first):

| string command    |  effect                   |       |
|---                |---                        |---    |
|start              |   shows logo of MCI       |       |
|gameover           |                           |       |