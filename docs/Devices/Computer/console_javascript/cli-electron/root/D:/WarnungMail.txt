____________________________Donnervogel____________________________________
VON: ingmar.damme@fresenius-labor.de
AN: hans.hövel@mariecurie-institut.de
BETREFF: Warnung
___________________________________________________________________________

Lieber Hans,

ich habe kürzlich in der "Nature" von Deinen Erfolgen gelesen. Dein 
Projekt scheint großen Fortschritt gemacht zu haben, das freut mich 
sehr zu hören! Arbeitest Du noch mit den beiden hübschen Doktorand-
innen zusammen, Aaron und McLain, wenn ich mich richtig erinnere? Du 
solltest aufpassen, Hans. Ich habe da etwas in Erfahrung gebracht, 
nur ein Gerücht natürlich, aber vielleicht solltest Du ihm ein 
bisschen Aufmerksamkeit schenken. Einige hier im Labor glauben, 
das Marie-Curie-Institut würde ausspioniert. Kannst Du Dir so etwas 
vorstellen! Wenn das stimmt, Hans… Halte Deine Ergebnisse besser 
unter Verschluss. Wer weiß, vielleicht unterhältst auch Du einen 
Maulwurf? Deine Forschungsarbeit ist so wichtig, bitte versprich mir, 
dass Du auf der Hut bist. 

Viel Forschergeist und Erkenntnisdrang wünscht Dein alter Freund,
Ingmar

___________________________________________________________________________