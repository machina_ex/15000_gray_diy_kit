Technische Rätselrelevante Requisiten in 15000 Gray
====================================================

>Willkommen auf der Baustelle!
>Um in die richtige Stimmung zu kommen, tun wir in diesem Bitbucket Repository so, als wäre alles fix und fertig Dokumentiert und würde zusammenpassen wie Labormaus und Radioaktiver Käse. Leider ist das mitnichten der Fall.
>Hab also Nachsicht mit der unvollständigkeit und genieße den Spaziergang durch diese wachsende verwinkelte Anleitung zum Bau eines Real Live Theater Games.
> bereits baufertig sind:
- die [Zeitbombe](./Zeitbombe) (Dokumentation noch englisch)
- die [Uhr](./Uhr) (noch nicht getestet, Dokumentation noch nicht fertig)
- das [Numpad](./Numpad)


Für den Bau der Devices, die in 15000 Gray eine Rolle spielen findest du hier alle Anleitungen. Wie haben uns in den Anleitungen für eine Kabellose Variante zum Einbinden der Geräte in des Gesamtsystem entschieden. Dafür kommen die sehr günstigen und WiFi fähigen [WEMOS Mikrokontroller](https://wiki.wemos.cc/products:d1:d1_mini) zum Einsatz.

Für den bau aller Devices solltest du über folgende Dinge bescheid wissen:

### Werkzeug

In den meisten fällen wirst du nicht auskommen ohne:

* Lötkolben
* Lötzinn
* Schneidwerkzeug (Kneifzange, Seitenschneider o.ä.)

Um die Aufbauten zu testen legen wir dir wärmstens ans Herz:

* Breadboard
* Jumperkabel


### Stromversorgung

Da wir hier ein Setup mit WiFi fähigen Mikrokontrollern vorschlagen liegt es natürlich nahe die Devices mit Batterien zu versorgen. Eine komfortabele und günstige Lösung sind Powerbanks, die du auch zum mobilen Aufladen deines Smartphones nutzt.
Denke nur beim Bau der Device Gehäuse, dass du noch eine Powerbank unterbringen musst und das du die Powerbanks zwischen den Aufführungen aufladen solltest. Alle 15000 Gray devices halten einige Stunden durch wenn du sie mit einer 2000mAh Powerbank versorgst.

###WEMOS besonderheiten

Um die Pins des WEMOS zu Adressieren benutzt du nicht die Nummer des Pins wie beim Arduino (z.B. 3 für Digital Pin 3) sondern setzt jeweils den Buchstaben D davor. Orientiere dich also an der Beschrifftung der Pins, nicht an der Nummerierung.
natürlich ist dieser kleine Unterschied in den Codes die du hier herunterladen kannst bereits berücksichtigt. Wenn du jedoch das Board wechselst oder Pin belegungen ändern möchtest, passe den Code ggf. an.


####Code aufspielen

Um deine Wemos Mikrokontroller mit der richtigen Software zu bespielen benötigst du die [Arduino IDE](https://www.arduino.cc/en/Main/Software) und musst für die Wemos Boards [zusätzliche Software](https://wiki.wemos.cc/tutorials:get_started:get_started_in_arduino?do=) installieren.

Um den WEMOS anschließend mit einem bestimmten Code zu bespielen öffne die *.ino* Datei des Devices mit der Arduino IDE Software.

Stecke deinen WEMOS an den Rechner und wähle den Wemos USB Port unter `Tools -> Ports` aus. Wenn da schon andere Sachen in der Liste auftauchen ziehe den Wemos wieder ab und gucke welcher Port aus der Liste verschwunden ist um herauszufinden wie sich das Wemos nennt.

Wähle unter `Tools -> Boards` dein Wemos Board (z.B. `Wemos D1 & mini` wenn du ein D1 da hast) aus.

Passe die WiFi Variablen im oberen Teil des Codes an:

```c
/* 
 *  Variablen fuer die Verbindung zum Netzwerk
 *  Setze hier deine WLAN Verbindungsdaten ein.
 */
const char* ssid = "deineWiFiID";
const char* password = "deinWiFiPasswort";

bool wifiConnected = false;


/* 
 *  Variablen fuer die Netzwerkkommunikation
 *  setze hier die IP Adressen abhaengig von deinen Netzwerkeinstellungen ein.
 */
IPAddress ip(192,168,178,112); // Die IP Adresse der Uhr
IPAddress gateway(192,168,178,1); // Die IP Adresse des Routers
IPAddress subnet(255,255,255,0); // Die Subnetzmaske. Fuer gewoehnlich (255,255,255,0)
IPAddress outIP (192,168,178,51); // Die IP Adresse der Zentralen Schnittstellen Software
```

Ersetze die Werte für `ssid`, `password` zwischen den Anführungszeichen.
Ersetze die Werte für `ip()`, `gateway()` und `outIP()` zwischen den Klammern.

Spiele den Code auf indem du auf den Upload Button -> drückst.

Öffne den `Serial Monitor` um den frischen Code zu überprüfen. Achte darauf, dass die Baudrate im Serial Monitor auf `115200` gesetzt ist.