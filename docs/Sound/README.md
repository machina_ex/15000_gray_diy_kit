SOUND
======

Die urspüngliche Inszenierung von 15.000 Gray verwendet Sounds, Hörspiele und
Musik zu vielfältigen Zwecken: Um Charaktere an dem anderen Ende eines
Telefonats zu behaupten, um einen Rückwärtslauf der Zeit zu plausibilisieren, um
das fortschreitende Ticken der Bombe zu unterstreichen, um Triumpf oder
Niederlage zu markieren, um eine grundsätzliche Atmosphäre herzustellen.

In der vorliegenden technischen Umsetzung liegen alle für das Stück nötigen
Sounddateien als Samples arrangiert in dem Programm Ableton Live. All diese
Samples werden im Verlauf des Stückes von dem zentralen Programm Max durch
die Weitergabe von Midinoten durch Max an Live automatisch abgespielt – die
Betreung des Ablaufes durch eine_n Techniker_in in Max erledigt auch alle Sound-
Ereignisse.

In der ursprünglichen Inszenierung gibt es folgende Soundquellen im Raum:

- eine Veranstaltungsanlage oder große Aktivboxen.
- ein altes Radio.
- den Hörer eines modifizierten [Telefons](../Devices/Telefon).
- eine kleine Aktivbox in der [Uhr](../Devices/Uhr).

Um die vorliegenden Livesets für eine Inszenierung von 15.000 GRAY mit Ableton
Live nutzen zu können, müssen einige Vorbereitungen getroffen werden:

####Technik

1. Ableton Live muss mindestens in der Version 9 auf dem Computer laufen.
2. Max muss mit Live per MIDI kommunizieren können.
3. Ableton Live muss per MIDI mit sich selbst kommunizieren können.
4. Ableton Live muss mit einer Soundkarte verbunden sein, die im Stande ist, die
Ausgabegeräte zu speisen.

####Inszenierung/Schauspiel

5. Das Hörspiel Radio Professor muss neu aufgenommen und eingefügt werden.
6. (optional) Die Hörspiele Radio Sprecher, Jörg, Jörg Lose und Jörg Win können
neu aufgenommen werden.

###1.

Unter https://www.ableton.com/ sind Versionen von Ableton Live zum Test und Kauf
verfügbar. Zum Stand Januar 2018 genügt die günstigste Version von Live – Live
Lite – zum Aufführen von 15.000 GRAY.

###2.

Um die MIDI-Kommunikation von Max zu Ableton Live sicherzustellen muss im
Maxpatch »15kGray_Main« in dem Reiter Sound from Max 1 als »MIDI_SENDER«
ausgewählt sein.

![001](img/001.png)

Außerdem muss Ableton Live so konfiguriert werden, dass es die von Max
ankommenden MIDI-Noten als Fernsteuerung akzeptiert. In den Einstellungen von
Ableton Live (in dem Programm zu erreichen über das Kontextmenü Live->
Voreinstellungen oder die Tastenkombination cmd+,) gibt es den Reiter »Link
MIDI«. In diesem Reiter muss der Input »from Max 1« für die Fernsteuerung von Live
aktiviert werden.

![002](img/002.png)

###3.

Im Verlaufe des Stückes sendet das Liveset von 15.000 GRAY mitunter MIDI-Noten
an sich selbst. In MacOS kann dies mit einem in das Betriebssystem eingebauten
Programm ermöglicht werden, dem IAC-Treiber. Dieses Programm erfüllt einen
einfachen Dienst: Es stellt einen digitalen MIDI-Eingang, über den es Noten
empfangen kann, die es über einen digitalen MIDI-Ausgang sofort wieder ausgibt.

1) Unter Programme -> Dienstprogramme findet sich ein Programm namens Audio-
MIDI-Setup. Nach dem Öffnen des Programmes lässt sich hierin mittels des
Kontextmenüs »Fenster« oder über die Tastenkombination cmd+2 das Midifenster
aufrufen.

![003](img/003.png)

2) Mit einem Doppelclick auf den möglicherweise ausgegrauten IAC-Treiber lässt
sich ein Konfigurationsfenster für denselben aufrufen. Hier muss die Option »Gerät
ist bereit« markiert sein, um den IAC-Treiber nutzbar zu machen.

![004](img/004.png)

3) In den Einstellungen von Ableton Live (in dem Programm zu erreichen über das
Kontextmenü Live->Voreinstellungen oder die Tastenkombination cmd+,) gibt es den
Reiter »Link MIDI«. In diesem Reiter muss der Input »IAC-Treiber (Bus 1)« für die
Fernsteuerung von Live aktiviert werden und der Output »IAC-Treiber (Bus 1)« für
Track aktiviert werden.

![005](img/005.png)

Der IAC-Treiber kann je nach Version des Betriebssystems und eingestellter
Sprache einen anderen Namen haben (etwa »IAC Driver«), das spielt für die
Funktion des Programms keine Rolle.
Unter Windows gibt es keinen im Betriebssystem vorgesehenen virtuellen MIDI-Bus,
es gibt allerdings freie Drittanbieter Software wie [MIDI-Ox](http://www.midiox.com/), die die Funktion
übernehmen.

###4.

Das Liveset von 15.000 Gray hat fünf Sound-Ausgänge vorgesehen:

- Kanal 1/2 für die Veranstaltungsanlage.
- Kanal 3 für das [Radio](../Ausstattung).
- Kanal 4 für den Hörer des [Telefons](../Devices/Telefon).
- Kanal 5 für die kleine Aktivbox in der [Uhr](../Devices/Uhr).

Um diesen Aufbau gewährleisten zu können, muss Ableton Live also mit einer
Soundkarte mit mindestens fünf Ausgängen verbunden werden. Diese Soundkarte
muss in den Voreinstellungen von Live im Reiter Audio als »Audio-Ausgangsgerät«
angewählt werden. Außerdem müssen die ersten fünf Kanäle der Soundkarte unter
»Ausgangskonfiguration« aktiviert werden – Kanal 1/2 als Stereo, Kanal 3, 4 und 5
als *Mono*.

![006](img/006.png)

Es empfiehlt sich, die Soundquellen mit massegetrennten Verbindungen mit der
Soundkarte zu verbinden, um Brummeinstreuungen durch andere Stromquellen auf
den langen Kabelwegen zu vermeiden.

###5.

In dem Radiohörspiel, dass die Erzählung von 15.000gray eröffnet, tritt die Rolle des
Professors als Befragter in einem Interview auf. Es ist damit die einzige Rolle, die
sowohl in Form eines Hörspiels auftritt, als auch in Person. Das originale Hörspiel
muss also ersetzt werden.

Zu diesem Zweck liegt ein weiteres Liveset in dem Projekt »Hörspiele 15.000 gray«
mit dem Dateinamen »radiohörspiel.als«. In der *Arrangement View* des Livesets sind
drei Spuren angelegt: Die Stimme des Sprechers, die Stimme des Professors und
das Sounddesign für das Radio. Die Stimme des Professors kann hier durch neue
Aufnahmen ersetzt werden. (Ich verzichte hier darauf, die Grundlagen von Aufnahme
und Schnitt in Ableton Live zu erklären, da Ableton selbst hierfür erschöpfende
Tutorials bereit stellt.)

Die fertige Aufnahme muss als einzelne Spur gerendert werden. Damit sie sich in
das Timing des originalen Sounddesigns einpasst, muss auch die Stille bis zum
Beginn des Hörspiels in der Ausspielung enthalten sein. Dieses kann zum Beispiel
erreicht werden, in dem man die auszuspielende Zeit mit der Loop-Klammer
markiert:

![007](img/007.png)

Nun kann in der Exportfunktion (erreichbar über Datei->Audio/Video exportieren oder
über cmd+shift+r) die Spur »Professor« ausgewählt. Nun kann im Liveset »Liveset
15k gray« die entsprechende Datei mit der gerenderten Datei ersetzt werden. Dabei
sollte die »Warp«-Funktion des Tracks ausgeschaltet werden.

###6.

Auch die anderen Hörspiele, das heißt die Stimme des Sprecher und drei Hörspiele
von Jörg (zu finden in der Hörspieldatei »telefonhörspiele.als«) können nach
ähnlichem Muster durch eigene Schauspieler_innen ersetzt werden.

![008](img/008.png)