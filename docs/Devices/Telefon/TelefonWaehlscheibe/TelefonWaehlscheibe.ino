/*
*  Weltkarte
 *  
 *  Die waehlscheibe ist am braunen und weissen draht zu verbinden. Die polaritaet
 *  ist nicht relevant.
 *  
 *************************************/

// UDP
/*
#include <MachinaUdp.h>
 MachinaUdp udp(8); // MachinaUDP Objekt fuer ARDUINO1
 
 #include <SPI.h>
 #include <Ethernet.h>
 #include <Z_OSC.h>
 */


int dialPin = 2; // Waehlscheibe auf DI 5
int dialVal = 0;
int dialValStat = 0;

int nummer = 0;
int taste = 10;

unsigned long int signalstart = 0;
unsigned long int signalstop = 0;

unsigned long int signalzeit = 0;
unsigned long int keinsignalzeit = 0;

unsigned long int auszeit = 200;
unsigned long int zwischenzeit = 100;

boolean dialing = false;
boolean aufgelegt = false;


void setup() 
{
  // Serial Schnittstelle inititalisieren
  Serial.begin(115200);

  Serial.println("Arduino");
  Serial.println("Neues Programm version 0.1");
  Serial.println();

  pinMode(dialPin, INPUT);

  /*
  udp.start();
   udp.setDestIp(3);
   */
}


void loop() 
{
  dialVal = digitalRead(dialPin);

  /*
  *  Wahlvorgang
   */

  if ( dialVal != dialValStat )
  {
    dialValStat = dialVal;

    // bei hochziehen der Waehlscheibe kommt einmalig ein Signal
    if (dialVal == HIGH)
    {
      dialing = true;
      if (aufgelegt)
      {
        Serial.print("telefon ");
        Serial.print("abgehoben");
        Serial.println();
        aufgelegt = false;
        signalstop = millis();
      }
    } 

    else if (dialVal == LOW)
    {
      signalstop = millis();

      //if (dialing)
      //{
      nummer++;
      Serial.print("telefon ");
      Serial.print("klackern");
      Serial.println();

      //udp.sendStr("/Dial", "klacker");
      //}
    }
  }

  /*
  *   Abfrage ob waehlscheibe runtergelaufen
   */
  keinsignalzeit = millis() - signalstop;
  
  if ( dialing == true )
  {

    if ( keinsignalzeit > zwischenzeit )
    {
      if (nummer == 10) 
      {
        taste = 0;
      } 
      else if (nummer > 0) {
        taste = nummer;
      }
      if (taste < 10) 
      {
        Serial.print("telefon ");
        Serial.print(taste);
        Serial.println();

        //udp.sendInt("/Dial", taste);
      }

      dialing = false;
      nummer = 0;
      taste = 10;
    }
  }

  /*
  *   abfrage ob hoerer aufgelegt
   */
  if ( dialVal == LOW )
  {
    if ( keinsignalzeit > auszeit && aufgelegt == false)
    {
      Serial.print("telefon ");
      Serial.print("aufgelegt");
      Serial.println();
      aufgelegt = true;
    }
  } 
}




