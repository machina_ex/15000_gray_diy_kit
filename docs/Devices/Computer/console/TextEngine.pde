//import java.awt.geom.*;

public class TextEngine 
{ LinkedList lineBuffer = new LinkedList(); 

  final int fontSize = 16; 
  final int maxBufferSize = 45; 
  final int maxLines = 35; 
  final int leftMargin = 10; 

  int scrollIndex = 0; 

  TextEngine() 
  { 
    textFont(loadFont("OCRAStd-10.vlw"),fontSize); 
  } 

  // Inhalt des neuen Textes festlegen
  /** 
   * Set the text being displayed in the window 
   */
  void setText(String[] text) 
  { 
    lineBuffer.clear(); 
    addText(text); 
  } 

  // Inhalte zum Text hinzufügen
  /** 
   * Add the strings to the line buffer 
   */
  void addText(String[] text) 
  { 
    for(int i = 0; i < text.length; i++) 
    { 
      addString(text[i]); 
    } 
  } 

  /** 
   * Add a string to the line buffer 
   */
  void addString(String text) 
  { 
    lineBuffer.add(text); 
    removeExcess(); 
  } 

  /** Den letzten String im Buffer ersetzen
   * Replace the last string of the line buffer 
   */
  //- added this method 
  void setLastString(String text) 
  { 
    if(lineBuffer.size() > 0) 
    { 
      lineBuffer.removeLast(); 
    } 
    addString(text); 
  } 

//Text ausgeben
  /**
   * Draw the visible lines on the screen
   */
  void draw() 
  { draw(max(0, lineBuffer.size()-maxLines));
   } 

  /**
   * Draw the visible lines on the screen
   * Starting from the given index in the line buffer
   */
  private void draw(int start) 
  { 
    background(0); 
    fill(0,255,0); 

    int nbLines = min(maxLines, lineBuffer.size()); 
    for(int i = start; i < start+nbLines ; i++) 
    { 
      String line = (String)lineBuffer.get(i%lineBuffer.size()); 
      text(line,leftMargin,((i-start)*fontSize)+fontSize);
    } 
    text (frameCount/10 % 2 == 0 ? "_" : "");
  } 
  
//Scrollen
 /**
   * Scroll the line buffer across the screen
   */
  void scrollText() 
  { 
    //println(scrollIndex);
    draw(scrollIndex);
    //println("condition is " + (lineBuffer.size() - scrollIndex)); 
    if(lineBuffer.size() - scrollIndex > maxLines) 
    { 
      scrollIndex = (scrollIndex + 1 % lineBuffer.size());
      //println("buffer size is " + lineBuffer.size());
    } 
  }

  void setScrollIndex()
  {
    scrollIndex = 0;
  }
  
  /**
   * Remove the first line if the buffer is larger than the 
   * maximum buffer size
   */

  private void removeExcess() 
  { 
    if(lineBuffer.size() > maxBufferSize) 
    { 
      lineBuffer.removeFirst(); 
    } 
  } 
}
