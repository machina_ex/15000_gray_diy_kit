Dokumente
=========

Hier bemühen wir uns alle Dokumente, die in 15000 Gray eine Rolle spielen zu sammeln. Manche kannst du einfach auf DINA 4 ausdrucken, andere müssen auf bestimmten Requisiten handschriftliche übertragen werden.

Aktenordner
-----------

- [alphazerfall.pdf](./Aktenordner/alphazerfall.pdf)
- [Radiodurans Artikel.pdf](<./Aktenordner/Radiodurans Artikel.pdf>)
- [Radiodurans Bilder.pdf](<./Aktenordner/Radiodurans Bilder.pdf>)
- [TABLE 2.pdf](<./Aktenordner/TABLE 2.pdf>)
- [TABLE 3.pdf](<./Aktenordner/TABLE 3.pdf>)
- [TABLE lang.pdf](<./Aktenordner/TABLE lang.pdf>)
- [Table of elements.pdf](<./Aktenordner/Table of elements.pdf>)

Mischrätsel
-----------

- [Rezept.txt](./Mischrätsel/Rezept.txt)

Whiteboard
----------
- [griechischlatein.jpg](./Whiteboard/griechischlatein.jpg)

![Whiteboard Foto Beispiel](<./Whiteboard/whiteboard photobeispiel.jpg>)


